package dalapo.invlink.logic;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableSet;

import dalapo.invlink.auxiliary.BackstuffInventory;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.auxiliary.Triplet;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.tile.TileEntityItemPipe;
import net.minecraft.client.MinecraftClient;
// import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.BlockPos;

// A PipeNode is a section of pipe that is either an intersection or connected to an inventory
// If there's anything to be learned from making this mod, it's that I need to take a data structures course :(
public class PipeNode
{
	private final long id;
	
	// Edges and immediate connections are stored in a HashMap instead of as PipeEdge objects to allow O(1) retrieval time for pathing
	private Map<PipeNode, Integer> edges = new HashMap<>();
	private Map<PipeNode, TileEntityItemPipe> immediateConnections = new HashMap<>();
	private Set<TileEntityItemPipe> edgePipes = new HashSet<>();
	private BiMap<Direction, NetworkedInventory> inventories = HashBiMap.create();
	private TileEntityItemPipe tile;
	private PipeNetwork network;
	
	// Fields used specifically for pathing
	private Map<PipeNode, Integer> distance = new HashMap<>(); // Dijkstra distance from origin
	private boolean hasCalculated = false;
	private boolean visited = false;
	private Map<PipeNode, PipeNode> predecessors = new HashMap<>(); //  K: Origin node / V: Immediate predecessor
	
	public PipeNode(TileEntityItemPipe pipe, PipeNetwork network)
	{
		this.id = pipe.getPos().asLong();
		this.tile = pipe;
		this.network = network;
	}
	
	public void initForPathing(PipeNode origin)
	{
		distance.put(origin, this == origin ? 0 : Integer.MAX_VALUE);
		visited = false;
		hasCalculated = false;
		predecessors.remove(origin);
	}
	
	public void clearPredecessors()
	{
		predecessors.clear();
		distance.clear();
		hasCalculated = false;
	}
	
	public void setHasCalculated(boolean calc)
	{
		hasCalculated = calc;
	}
	
	public boolean hasCalculated()
	{
		return hasCalculated;
	}
	
	public void setNetwork(PipeNetwork network)
	{
		this.network = network;
	}
	
	public void setDistance(PipeNode origin, int dist)
	{
		distance.put(origin, dist);
	}
	
	public int getDistance(PipeNode origin)
	{
		return distance.containsKey(origin) ? distance.get(origin) : Integer.MAX_VALUE;
	}
	
	public void setVisited(boolean visited)
	{
		this.visited = visited;
	}
	
	public boolean isVisited()
	{
		return visited;
	}
	
	public void setPredecessor(PipeNode origin, PipeNode other)
	{
		predecessors.put(origin, other);
	}
	
	public PipeNode getPredecessor(PipeNode origin)
	{
		return predecessors.get(origin);
	}
	
	public PipeNetwork getNetwork()
	{
		return network;
	}
	
	// Returns true if a new inventory was added successfully.
	public boolean addInventory(Direction f, NetworkedInventory inv)
	{
		return inventories.put(f, inv) == null;
	}
	
	public Set<NetworkedInventory> getConnectedInventories()
	{
		return ImmutableSet.copyOf(inventories.inverse().keySet());
	}
	
	public Set<Direction> getInventoryDirections()
	{
		return ImmutableSet.copyOf(inventories.keySet());
	}
	
	// Returns an attached inventory that will accept at least some of the given item, or null if there is none
	public NetworkedInventory getValidInventory(ItemStack is)
	{
		for (NetworkedInventory inv : inventories.inverse().keySet())
		{
			if (!(inv.inventory instanceof BackstuffInventory) && !InventoryHelper.areItemStacksIdentical(InventoryHelper.tryInsertItem(inv.inventory, is, true), is)) return inv;
		}
		return null;
	}
	
	public NetworkedInventory getBackstuffInventory(ItemStack is)
	{
		for (NetworkedInventory inv : inventories.inverse().keySet())
		{
			if (inv.inventory instanceof BackstuffInventory) return inv;
		}
		return null;
	}
	
	public void removeInventory(Direction f)
	{
		inventories.remove(f);
	}
	
	public NetworkedInventory getInventory(Direction f)
	{
		return inventories.get(f);
	}
	
	// Calculates the edges that connect to this node.
	// Returns a pair representing the neighbour node, and this node's distance to it.
	private Pair<PipeNode, Integer> calcConnectionHelper(TileEntityItemPipe current, TileEntityItemPipe prev, Set<TileEntityItemPipe> visited, int distance)
	{
		if (current.isNode() && current != tile) // A neighbour node has been found. Add an edge in the network and return it.
		{
			PipeNode target = network.findNode(current);
			target.addEdge(this, prev, distance + 1);
			network.putEdge(visited, this, target);
			return new Pair<>(target, distance + 1);
		}
		
		visited.add(current);
		edgePipes.add(current);
		
		Set<TileEntityItemPipe> pipes = current.getConnectedPipes();
		for (TileEntityItemPipe pipe : pipes) // Only iterates once but means we don't have to keep track of directions
		{
			// Recurse to the next pipe, propagating down
			if (pipe != tile && !visited.contains(pipe)) return calcConnectionHelper(pipe, current, visited, distance + 1);
		}
		
		// If execution reaches this point, a dead end has been reached. Mark it as such and return null
		visited.forEach(pipe -> {
			pipe.setDeadEnd(this);
			edgePipes.add(pipe);
		});
		return null;
	}
	
//  Legacy method, uncomment to restore
//	private Pair<PipeNode, Integer> calcConnectionHelper(TileEntityItemPipe current, Set<TileEntityItemPipe> visited, int distance)
//	{
//		visited.add(current);
//		Set<TileEntityItemPipe> pipes = current.getConnectedPipes();
//		for (TileEntityItemPipe pipe : pipes)
//		{
//			if (pipe != tile && !visited.contains(pipe))
//			{
//				if (pipe.isNode())
//				{
//					PipeNode target = network.findNode(pipe);
//					target.addEdge(this, current, distance + 1);
////					target.immediateConnections.put(this, current); // May be an issue with multiple paths
//					network.putEdge(visited, this, target);
//					return new Pair<>(target, distance + 1);
//				}
//				else
//				{
//					edgePipes.add(current);
//					return calcConnectionHelper(pipe, visited, distance + 1);
//				}
//			}
//		}
//		visited.forEach(pipe -> {
//			pipe.setDeadEnd(this);
//			edgePipes.add(pipe);
//		});
//		return null;
//	}
	
	// "Immediate connection" refers to the pipe tile that is immediately adjacent to this node, and is part of the edge
	// connecting this node to the passed in node. Used in item routing to send items the right way when they reach this node.
	public TileEntityItemPipe getImmediateConnection(PipeNode node)
	{
		return immediateConnections.get(node);
	}

	// As above, but returns the direction of the immediate connection
	public Direction getImmediateDirection(PipeNode node)
	{
		TileEntityItemPipe immediate = getImmediateConnection(node);
		return InvLinkMathHelper.getRelativeDirection(tile.getPos(), immediate.getPos());
	}
	
	// Calculates the connection of the edge going in direction f
	public Pair<PipeNode, Integer> calcConnection(Direction f)
	{
		return calcConnectionHelper(tile.getConnectedPipe(f), tile, new HashSet<>(), 0);
	}
	
	public void calcConnection(PipeNode p, boolean reciprocate)
	{
		removeEdge(p);
		calcConnectionsEfficiently();
		if (reciprocate)
		{
			p.calcConnectionsEfficiently();
		}
	}
	
	// As calcConnections, but if an adjacent pipe is already part of a valid edge, does not recalculate it.
	// Idea here is that to recalc a specific edge (as is often needed), removeEdge will be called before this function.
	// Still doesn't work quite right.
	public void calcConnectionsEfficiently()
	{
		for (Direction f : tile.getConnectionDirections())
		{
			if (!immediateConnections.containsValue(tile.getConnectedPipe(f))) // Don't recalc the connection if it's still there.
			{
				Pair<PipeNode, Integer> edge = calcConnection(f);
				if (edge != null)
				{
					addEdge(edge.a, tile.getConnectedPipe(f), edge.b);
				}
			}
		}
//		network.markNodeDirty(this);
	}
	
	// Invalidates *all* edges connected to this node, and then recalculates them.
	// Works, but is sort of a nuclear option and very inefficient. I don't like this code very much. Must change.
	public void calcConnections()
	{
		edges.clear();
		edgePipes.clear();
		immediateConnections.clear();
		for (Direction f : tile.getConnectionDirections())
		{
			Pair<PipeNode, Integer> edge = calcConnection(f);
			if (edge != null)
			{
				immediateConnections.put(edge.a, tile.getConnectedPipe(f));
				addEdge(edge.a, tile.getConnectedPipe(f), edge.b);
//				edge.a.addEdge(this, edge.b);
			}
		}
		network.markNetworkDirty();
//		network.markNodeDirty(this);
	}
	
	public Set<PipeNode> getNeighbours()
	{
		return edges.keySet();
	}
	
	public Set<TileEntityItemPipe> getEdgePipes()
	{
		return edgePipes;
	}
	
	public void addEdge(PipeNode other, TileEntityItemPipe immediate, int distance)
	{
		if (other != this && (!edges.containsKey(other) || distance < edges.get(other)))
		{
			immediateConnections.put(other, immediate);
			edges.put(other, distance);
		}
	}
	
	public void removeEdgeSimple(PipeNode other)
	{
		edges.remove(other);
		other.edges.remove(this);
		immediateConnections.remove(other);
		other.immediateConnections.remove(this);
		edgePipes.removeIf(pipe -> pipe.getNodes().a == other || pipe.getNodes().b == other);
		other.edgePipes.removeIf(pipe -> pipe.getNodes().a == this || pipe.getNodes().b == this);
	}
	
	// Only exists because I'm lazy and don't want to update all the removeEdge calls in other methods
	private void removeEdge(PipeNode other)
	{
		removeEdgeSimple(other);
//		network.edges.remove(this, other);
	}
	
	public int getImmediateDistance(PipeNode other)
	{
		if (!edges.containsKey(other)) return -1;
		return edges.get(other);
	}
	
	public TileEntityItemPipe getTile()
	{
		return tile;
	}
	
	// Necessary to avoid duplicate entries in the HashMaps. Nodes are unique per BlockPos, so this works.
	// Ideally the hash would be generated based on a combination of network id and node id, but this is ok.
	@Override
	public int hashCode() 
	{
		return tile.getPos().hashCode();
	}

	@Override
	public boolean equals(Object other)
	{
		return (other == this) || (other instanceof PipeNode && ((PipeNode)other).id == id);
	}
	
	public String toString()
	{
		return "NODE at " + tile.getPos().toString();
	}
}
