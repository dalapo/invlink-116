package dalapo.invlink.logic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableSet;

import dalapo.invlink.InventoryLink;
import dalapo.invlink.auxiliary.BackstuffInventory;
import dalapo.invlink.auxiliary.ItemHandler;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.BlockPos;

public class NetworkedInventory implements INetworkedObject
{
	private static final Set<Direction> ALL_SIDES = new HashSet<>();
	public final ItemHandler inventory;
	public PipeNetwork network;
	private final int id;
	private BlockPos pos;
	private BiMap<Direction, PipeNode> connections = HashBiMap.create(6);
	
	public NetworkedInventory(ItemHandler inv, BlockPos pos, PipeNetwork net)
	{
		this.inventory = inv;
		this.network = net;
		this.pos = pos;
		id = InventoryLink.random.nextInt();
	}
	
	public PipeNetwork getNetwork()
	{
		return network;
	}
	
	public void addNode(Direction f, PipeNode node)
	{
		connections.put(f, node);
	}
	
	public Direction getDirectionFromNode(PipeNode node)
	{
//		Logger.info(connections.size());
		return connections.
				inverse().
				get(node).
				getOpposite();
	}
	
	public void removeNode(PipeNode node)
	{
		connections.inverse().remove(node);
	}
	
	public void clearNodes()
	{
		connections.clear();
	}
	
	public Set<PipeNode> getNodes()
	{
		return ImmutableSet.copyOf(connections.inverse().keySet());
	}
	
	public void setNetwork(PipeNetwork net)
	{
		this.network = net;
	}
	
	public BlockPos getPos()
	{
		return pos;
	}
	
	public int hashCode()
	{
		return inventory.hashCode();
	}
	
	public String toString()
	{
		return String.format("%s at " + pos.toString(), inventory instanceof BackstuffInventory ? "BACKSTUFF" : "INVENTORY");
	}

	@Override
	public boolean canConnectToSide(Direction side)
	{
		return true;
	}
}