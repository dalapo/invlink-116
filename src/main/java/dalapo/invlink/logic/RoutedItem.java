package dalapo.invlink.logic;

import java.util.LinkedList;
import dalapo.invlink.auxiliary.BackstuffInventory;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.auxiliary.ItemHandler;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.helper.NetworkHelper;
import dalapo.invlink.tile.TileEntityItemPipe;
import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.BlockPos;

public class RoutedItem
{
	public static final int TICKS = 8; // Time taken for item to go from edge to centre or centre to edge

	private int age;
	private int id;
	private boolean hasID = false;
	private Direction currentDir;
	private Direction nextDir;
	private TileEntityItemPipe currentPipe;
	private ItemStack item;
	private boolean isDirty = false;
	private boolean isBackstuffing = false;
	private LinkedList<PipeNode> nodeSteps;
	private LinkedList<Direction> steps;
	private Pair<PipeNode, Direction> destination = new Pair<>();
	
	public RoutedItem(Direction initialDir, Pair<PipeNode, Direction> dest, ItemStack is)
	{
		this.item = is;
		this.currentDir = initialDir;
		this.destination = dest;
		age = -TICKS;
	}
	
	public RoutedItem(TileEntityItemPipe p, CompoundTag nbt)
	{
		setPipe(p);
		readFromNBT(nbt);
	}
	
	public void setID(int id)
	{
		if (!hasID) this.id = id; // Make ID pseudo-final without enforcing setting a value on the constructor
		hasID = true;
	}
	
	public int getID()
	{
		return id;
	}
	
	public boolean hasID()
	{
		return hasID;
	}
	
	public void setPath(LinkedList<PipeNode> path)
	{
		nodeSteps = path;
		setPathDirections(NetworkHelper.convertNodeList(path));
	}

	public void setPathDirections(LinkedList<Direction> path)
	{
		this.steps = path;
//		Logger.info(String.format("%s, %s", nodeSteps.size(), steps.size()));
	}
	
	public void markDirty()
	{
		isDirty = true;
	}
	
	public void markDirtyIfContains(PipeNode p)
	{
		if (nodeSteps.contains(p)) markDirty();
	}
	
	private void recalcSteps()
	{
		Pair<PipeNode, Direction> dest = currentPipe.getNetwork().findSuitableDestination(currentPipe.getNodes().a, item, true);
		if (dest != null)
		{
			this.destination = dest;
			setPath(currentPipe.getNetwork().pathNodal(currentPipe.getNodes().a, dest.a));
		}
		isDirty = false;
//		updateDirections();
	}
	
	/**
	 * something something do not modify the returned itemstack
	 */
	public ItemStack getItem()
	{
		return item;
	}
	
	public LinkedList<Direction> getSteps()
	{
		return steps;
	}
	
	public BlockPos getPos()
	{
		return currentPipe.getPos();
	}
	
	public void setPipe(TileEntityItemPipe pipe)
	{
		currentPipe = pipe;
	}
	
	public Direction getDir()
	{
		return currentDir;
	}
	
	public Direction getNext()
	{
		return nextDir;
	}
	
	public void updateDirections()
	{
		if (currentPipe.shouldBeNode())
		{
			if (isDirty)
			{
				recalcSteps();
			}
			
			if (destination == null)
			{
				for (Direction f : Direction.values())
				{
					if (currentPipe.getConnectionDirections().contains(f))
					{
						nextDir = f;
						break;
					}
				}
			}
			else
			{
				if (currentPipe.getNodes().a != destination.a && !nodeSteps.isEmpty())
				{
	//				Logger.info(nodeSteps);
	//				nextDir = null;
					nextDir = currentPipe.getNodes().a.getImmediateDirection(nodeSteps.pop());
				}
				else nextDir = destination.b;
			}
		}
		else
		{
			nextDir = currentPipe.getOtherConnection(currentDir.getOpposite()); // Breaks with teleporters
		}
	}
	
	// Returns true if the item should be removed from the pipe.
	// Only runs server-side.
	public boolean tick()
	{
		if (++age >= TICKS)
		{
			age = -TICKS;
			if (currentPipe.isNode() && currentPipe.getNodes().a == destination.a)
			{
				ItemHandler inv = currentPipe.getInventories().get(destination.b);
				if (inv instanceof BackstuffInventory)
				{
					((BackstuffInventory)inv).addItem(item, false);
					return true;
				}
				else
				{
					item = InventoryHelper.tryInsertItem(inv, item, false);
					if (!item.isEmpty())
					{
						currentDir = destination.b.getOpposite();
						isDirty = true;
						currentPipe.addItem(this);
						return false;
					}
					else return true;
				}
			}
			
			if (nextDir != null)
			{
				if (currentPipe.hasTeleport(nextDir)) currentDir = currentPipe.getTeleportOutputDirection(nextDir);
				else currentDir = nextDir;
//				currentDir = nextDir;
				TileEntityItemPipe nextPipe = currentPipe.getConnectedPipe(nextDir);
				if (nextPipe == null && !currentPipe.getWorld().isClient)
				{
					ItemEntity ei = new ItemEntity(currentPipe.getWorld(), currentPipe.getPos().getX()+0.5, currentPipe.getPos().getY()+0.5, currentPipe.getPos().getZ()+0.5, item);
					currentPipe.getWorld().spawnEntity(ei);
				}
				else currentPipe.getConnectedPipe(nextDir).addItem(this);
			}
			else
			{
				currentPipe.addItem(this);
			}
			return true;
		}
		return false;
	}
	
	public int getAge()
	{
		return age;
	}
	
	public CompoundTag writeToNBT(CompoundTag nbt)
	{
		nbt.putInt("id", id);
		nbt.putInt("age", age);
		nbt.putInt("dir", currentDir.getId());
		nbt.putInt("next", nextDir.getId());
		nbt.putInt("destdir", destination.b.getId());
		nbt.putLong("destnode", destination.a.getTile().getPos().asLong());
//		ListTag list = new ListTag();
//		for (Direction f : steps)
//		{
//			list.appendTag(new IntTag(f.getIndex()));
//		}
//		nbt.setTag("path", list);
		CompoundTag compound = item.getOrCreateTag();
		nbt.put("item", compound);
		return nbt;
	}
	
	public void readFromNBT(CompoundTag nbt)
	{
		setID(nbt.getInt("id"));
		age = nbt.getInt("age");
		currentDir = Direction.byId(nbt.getInt("dir"));
		nextDir = Direction.byId(nbt.getInt("next"));
		steps = new LinkedList<>();
		nodeSteps = new LinkedList<>();
		destination = new Pair<>(null, Direction.byId(nbt.getInt("destdir"))); // look away, look away
		isDirty = true;
//		ListTag list = nbt.getTagList("path", 3);
//		steps = new LinkedList<>();
//		for (int i=0; i<list.tagCount(); i++)
//		{
//			steps.add(Direction.getFront(list.getIntAt(i)));
//		}
		item = ItemStack.fromTag((CompoundTag)nbt.get("item"));
	}

}