package dalapo.invlink.logic;

import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Direction;

public class RoutedItemClient
{
	public final ItemStack is;
	public final Direction entryDir;
	public final Direction exitDir;
	public int age;
	
	public RoutedItemClient(ItemStack is, Direction entry, Direction exit)
	{
		this.is = is;
		entryDir = entry;
		exitDir = exit;
		age = -RoutedItem.TICKS;
	}
	
	public boolean tick()
	{
		return ++age > RoutedItem.TICKS;
	}
}