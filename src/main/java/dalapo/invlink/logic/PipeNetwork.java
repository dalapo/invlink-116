package dalapo.invlink.logic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.ImmutableSet;

import java.util.PriorityQueue;
import java.util.Set;
import java.util.function.Function;

import dalapo.invlink.InventoryLink;
import dalapo.invlink.auxiliary.ItemHandler;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.helper.CollectionHelper;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.tile.TileEntityItemPipe;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
// import net.minecraftforge.items.ItemHandler;

/**
 * A pipe network is implemented as an undirected graph.
 * A pipe is a NODE iff one or more of the following statements are true:
 * - The pipe connects to at least three other pipes to form an intersection, where an incoming item would have to make a decision on which exit to take
 * - The pipe connects to at least one inventory, making it a valid source or sink for items in the network.
 * A pipe is an EDGE iff it is part of an unbroken connection of pipes between two nodes.
 * A pipe that is not a NODE or an EDGE is considered a DEAD END.
 * 
 * Notes for later:
 * - If a network is modified after nodes have calculated paths, any paths containing the modified node (if a node was modified) or two nodes of an
 * edge (if an edge) will need to be recalculated. Depending on the overhead of pathing it may be more convenient to simply wipe all
 * current paths and recalc everything.
 * - Any items traveling through a network that is modified may have their paths become invalid. All items therefore must be notified
 * of a network change so that they can recalculate their paths.
 * @author davidpowell
 */
public class PipeNetwork
{
	// This code doesn't smell very nice at all.
	
	public Set<TileEntityItemPipe> allPipes = new HashSet<>();
	public BiMap<Integer, PipeNode> allNodes = HashBiMap.create();
	private Set<NetworkedInventory> allInventories = new HashSet<>();
	private Set<RoutedItem> allItems = new HashSet<>();
	public final int id; // For debugging
	
	private int nextNodeId = 0;
	private int nextItemId = 0;
	private int nextInvId = 0;
	
	public PipeNetwork()
	{
		id = InventoryLink.random.nextInt();
	}
	
	// Initialize the network. Go through and create all nodes, and then calculate their connections for the edges.
	// Should be called by TileEntityItemPipe after all pipes have been added, but before nodes and edges have been calculated.
	public void init()
	{
		for (TileEntityItemPipe pipe : allPipes)
		{
			if (pipe.shouldBeNode())
			{
				PipeNode node = new PipeNode(pipe, this);
				allNodes.put(nextNodeId++, node);
				pipe.setNode(node);
				pipe.getInventories().forEach((dir, inv) -> addInventory(node, dir, inv));
			}
		}
		allNodes.inverse().keySet().forEach(node -> node.calcConnections());
	}
	
	// Invalidates all cached paths in the network, forcing item routing to recalculate.
	public void markNetworkDirty()
	{
		allNodes.inverse().keySet().forEach(node -> node.clearPredecessors());
		allItems.forEach(item -> item.markDirty());
	}
	
	// If an inventory already exists at the given spot, remove the old one first.
	public void addInventory(PipeNode node, Direction dir, ItemHandler inventory)
	{
		if (node.getInventory(dir) != null) removeInventory(node, dir);
		NetworkedInventory inv = new NetworkedInventory(inventory, node.getTile().getPos().offset(dir), this);
		if (node.addInventory(dir, inv)) inv.addNode(dir.getOpposite(), node);
		allInventories.add(inv);
		markNetworkDirty();
	}
	
	public void removeInventory(PipeNode node, Direction f)
	{
		NetworkedInventory inv = node.getInventory(f);
		if (inv != null)
		{
			inv.getNodes().forEach(n -> n.removeInventory(f));
			allInventories.remove(inv);
		}
		markNetworkDirty(); // To be fair, it would probably be easier to just invalidate paths going to the given inventory.
	}
	
	public Set<NetworkedInventory> getConnectedInventories(PipeNode node) // Can't be done in PipeNode due to allInventories access
	{
		return node.getConnectedInventories();
	}
	
	private void putNode(PipeNode node)
	{
		allNodes.forcePut(nextNodeId++, node);
	}
	
	// Called when an existing pipe becomes a node; for example, when a new pipe is placed to branch off from an edge.
	public void addNewNode(TileEntityItemPipe pipe)
	{
		// If the pipe is part of an existing edge, temporarily remove the edge. New edges will be defined later in this method
		if (pipe.isEdge())
		{
			pipe.getNodes().a.removeEdgeSimple(pipe.getNodes().b);
		}
		// Create a new node, set the pipe tile to it, and add it to the registry
		PipeNode node = new PipeNode(pipe, this);
		pipe.setNode(node);
		putNode(node);
		pipe.getInventories().forEach((dir, inv) -> addInventory(node, dir, inv));
		node.calcConnections(); // Calculate and add the newly created edges
		allItems.forEach(item -> item.markDirty());
		node.getNeighbours().forEach(n -> n.calcConnections());
	}
	
	/**
	 * For when a pipe node becomes a regular pipe (for example, when an inventory is removed or an intersection loses one of its exits)
	 * @param pipe
	 */
	public void nodeToEdge(TileEntityItemPipe pipe)
	{
		if (findNode(pipe) != null) // Sanity check
		{
			PipeNode node = findNode(pipe);
			// We know that there cannot be more than two neighbours because edge removal runs before nodeToEdge
			List<PipeNode> neighbours = new ArrayList<>(2); 
			neighbours.addAll(node.getNeighbours());
			// This if statement only updates the edges from the pipes' perspective. Could be modified to update the network too.
			if (neighbours.size() == 2) // Pipe is part of an edge
			{
				pipe.setEdge(neighbours.get(0), neighbours.get(1));
			}
			else if (neighbours.size() == 1) // Pipe is just a dead end
			{
				pipe.setDeadEnd(neighbours.get(0));
			}
			// Recalc the edges from network's perspective
			neighbours.forEach(n -> {
				n.calcConnection(node, false);
				markNodeDirty(n);
//				n.removeEdgeSimple(node);
//				n.calcConnectionsEfficiently();
			});
		}
	}
	
	// Called when a pipe block is destroyed, and the pipe is a node
	public void destroyNode(TileEntityItemPipe pipe, boolean broken)
	{
		PipeNode node = findNode(pipe);
		allNodes.inverse().remove(node);
		pipe.setDead();
		Set<PipeNode> neighbours = new HashSet<>();
		neighbours.addAll(node.getNeighbours()); // Have to do it indirectly to prevent a CME
		neighbours.forEach(n -> {
			n.calcConnections();
		});
		allItems.forEach(item -> item.markDirty());
	}

	// If the pipe tile passed in is a node, return the node representation. Otherwise return null
	public PipeNode findNode(TileEntityItemPipe pipe)
	{
		if (pipe.isNode()) return pipe.getNodes().a;
		return null;
	}
	
	// As above, but takes a BlockPos. Does not call world.getTileEntity so can be used during init
	public PipeNode findNode(BlockPos pos)
	{
		for (PipeNode node : allNodes.inverse().keySet())
		{
			if (node.getTile().getPos().equals(pos)) return node;
		}
		return null;
	}
	
	// As above, but finds the edge that the pipe belongs to. Returns a pair representing the two nodes it connects.
	public Pair<PipeNode, PipeNode> getEdge(TileEntityItemPipe pipe)
	{
		if (pipe.isEdge()) return pipe.getNodes();
		return null;
	}
	// Called when two nodes are connected. If nodes belong to different networks, merge them.
	public void putEdge(Set<TileEntityItemPipe> members, PipeNode a, PipeNode b)
	{
		members.forEach(pipe -> pipe.setEdge(a, b));
		if (a.getNetwork() != b.getNetwork()) a.getNetwork().mergeNetwork(b.getNetwork());
	}
	
	// Verifies that there is an edge between a and b. Create one if there should be one and isn't. If there is not,
	// and there is no path from a to b, split the network.
	public void verifyEdge(PipeNode a, PipeNode b, boolean testSplit)
	{
//		Set<TileEntityItemPipe> toRemove = new HashSet<>();
//		for (TileEntityItemPipe p : allPipes)
//		{
//			if (p.isEdge() && p.getNodes().isEquivalent(a, b)) toRemove.add(p);
//		}
		a.calcConnection(b, true);
//		a.removeEdgeSimple(b);
//		a.calcConnectionsEfficiently();
//		b.calcConnectionsEfficiently();
		if (testSplit && pathNodalQuick(a, b) == null) splitNetwork(a, b);
		allItems.forEach(item -> item.markDirty());
	}
	
	public void verifyEdge(Pair<PipeNode, PipeNode> nodes, boolean testSplit)
	{
		verifyEdge(nodes.a, nodes.b, testSplit);
	}
	
	// Generic tile removal method
	public void removeTile(TileEntityItemPipe pipe)
	{
		allPipes.remove(pipe);
		if (pipe.isNode()) destroyNode(pipe, true);
		else if (pipe.isEdge())
		{
			pipe.getNodes().a.calcConnection(pipe.getNodes().b, true); // Shouldn't this be verifyEdge? Probably. Do I dare change it? No way.
		}
	}
	
	// Merges the passed in network into this one.
	public void mergeNetwork(PipeNetwork other)
	{
		other.allPipes.forEach(pipe -> pipe.setNetwork(this));
		allPipes.addAll(other.allPipes);
		other.allNodes.inverse().keySet().forEach(node -> node.setNetwork(this));
		other.allNodes.inverse().keySet().forEach(node -> putNode(node));
		other.allInventories.forEach(inv -> inv.setNetwork(this));
		allInventories.addAll(other.allInventories);
	}
	
	/**
	 * Remove a subset of nodes from this network and return them as a new network.
	 * A private method because the node set must be contiguous and part of this network.
	 * Needs optimization (how do subnets work again?)
	 * @param nodes The set of nodes to split off.
	 * @return The new network.
	 */
	private PipeNetwork splitHelper(PipeNode currentNode, PipeNetwork workingNetwork)
	{
		workingNetwork.putNode(currentNode);
		workingNetwork.allPipes.add(currentNode.getTile());
		workingNetwork.allPipes.addAll(currentNode.getEdgePipes());
		// This works because edges are removed before splitNetwork is called, and thus it is not possible to path
		// from the given node in network A to the one in network B. It is therefore safe to recursively call splitHelper
		// on all nodes it *is* possible to path to from each given node.
		for (PipeNode neighbour : currentNode.getNeighbours())
		{
			if (!workingNetwork.allNodes.containsValue(neighbour))
			{
				splitHelper(neighbour, workingNetwork);
			}
		}
		return workingNetwork;
	}
	
	private void enforceLoyalty()
	{
		allPipes.forEach(pipe -> pipe.setNetwork(this));
		allNodes.inverse().keySet().forEach(node -> node.setNetwork(this));
	}
	
	// Split the network into two subnets.
	// Arguments are one node from each new subnet. Nodes must not be able to reach each other
	// This code seems really bad and likely to cause memory leaks.
	public void splitNetwork(PipeNode a, PipeNode b)
	{
		PipeNetwork subnetA = splitHelper(a, new PipeNetwork());
		PipeNetwork subnetB = splitHelper(b, new PipeNetwork());
		subnetA.enforceLoyalty();
		subnetB.enforceLoyalty();
		//		subnetA.allNodes.forEach(node -> {
//			node.calcConnections();
//		});
//		subnetB.allNodes.forEach(node -> {
//			node.calcConnections();
//		});
	}

	// Propagates down the network, one pipe tile at a time, until a node is found. If a dead end is reached instead, return null
	private PipeNode propagateHelper(TileEntityItemPipe current, Set<TileEntityItemPipe> visited)
	{
		visited.add(current);
		if (current.isNode())
		{
			return current.getNodes().a;
		}
		for (TileEntityItemPipe pipe : current.getConnectedPipes())
		{
			if (!visited.contains(pipe)) return propagateHelper(pipe, visited);
		}
		return null;
	}
	
	// Add a new pipe to the network.
	public void addPipe(TileEntityItemPipe pipe)
	{
		pipe.setNetwork(this);
		allPipes.add(pipe);
		// Pipe fits criteria for a node, so add a new node.
		if (pipe.shouldBeNode())
		{
			addNewNode(pipe);
		}
		// Pipe connects to exactly 2 other pipes. Could be joining two nodes and therefore creating a new edge.
		else if (pipe.getConnectedPipes().size() == 2)
		{
			Set<PipeNode> nodes = new HashSet<>();
			for (Direction f : pipe.getConnectionDirections())
			{
				// Propagate down the network in both directions.
				Set<TileEntityItemPipe> pipes = new HashSet<>();
				pipes.add(pipe);
				PipeNode node = propagateHelper(pipe.getConnectedPipe(f), pipes);
				if (node != null) nodes.add(node);
			}
			if (nodes.size() == 2) // If a node was found at both end, update their calculations to get the new edge.
			{
				nodes.forEach(node -> node.calcConnections()); // Yeah, it's a bit inefficient :(
			}
		}
		else
		{
			for (Direction f : pipe.getConnectionDirections()) // Dead end. This for loop will only run once but iterating is still the only way to get the element from a 1-elem set
			{
				Set<TileEntityItemPipe> pipes = new HashSet<>();
				pipes.add(pipe);
				pipe.setDeadEnd(propagateHelper(pipe.getConnectedPipe(f), pipes));
			}
		}
	}
	
	// Quick-and-dirty depth-first pathing algorithm from one node to another. Used when finding the shortest distance isn't important.
	@SuppressWarnings("unchecked")
	private LinkedList<PipeNode> pathNodalDFS(PipeNode current, PipeNode dest, LinkedList<PipeNode> path)
	{
		path.add(current);
		if (current.equals(dest))
		{
			return path;
		}
		else
		{
			for (PipeNode next : current.getNeighbours())
			{
				if (!path.contains(next)) return pathNodalDFS(next, dest, (LinkedList<PipeNode>)path.clone());
			}
		}
		return null;
	}
	
	// Add an item to the network
	public void addItem(RoutedItem item)
	{
		int itemID = item.hasID() ? item.getID() : nextItemId++;
		item.setID(itemID);
		allItems.add(item);
	}
	
	// Remove the item whose ID corresponds to the given parameter
	public void removeItem(int id)
	{
		allItems.removeIf(item -> item.getID() == id);
	}
	
	// Return the item whose ID corresponds to the given parameter. Modifying the ItemStack is not recommended.
	public RoutedItem getItem(int id)
	{
		for (RoutedItem item : allItems)
		{
			if (item.getID() == id) return item;
		}
		return null;
	}
	
	/**
	 * Returns a list of PipeNodes to go from origin to dest. Simple DFS that should only be used to determine if a path exists.
	 * @param origin
	 * @param dest
	 * @return
	 */
	public LinkedList<PipeNode> pathNodalQuick(PipeNode origin, PipeNode dest)
	{
		return pathNodalDFS(origin, dest, new LinkedList<>());
	}
	
	// Comparator used for Dijkstra's algorithm.
	private static int compareDistance(PipeNode n1, PipeNode n2, PipeNode origin)
	{
		return n1.getDistance(origin) - n2.getDistance(origin);
	}
	
	// Not used
//	private static int taxicabDistanceSquare(PipeNode p1, PipeNode p2)
//	{
//		Vec3i diff = p1.getTile().getPos().subtract(p2.getTile().getPos());
//		return diff.getX()*diff.getX() + diff.getY()*diff.getY() + diff.getZ()*diff.getZ();
//	}
	
	/**
	 * Dijkstra's algorithm implementation to find the shortest path from origin to all other nodes.
	 * Currently has a lot of overhead since it calcs many paths - possible to take advantage of this?
	 * @param origin
	 * @param dest
	 * @return
	 */
	private void pathNodalDijkstra(PipeNode origin)
	{
		allNodes.inverse().keySet().forEach(node -> node.initForPathing(origin));
		PriorityQueue<PipeNode> queue = new PriorityQueue<>((n1, n2) -> PipeNetwork.compareDistance(n1, n2, origin));
		queue.add(origin);
		origin.setVisited(true);
		while (!queue.isEmpty())
		{
			PipeNode workingNode = queue.poll();
			for (PipeNode neighbour : workingNode.getNeighbours())
			{
				if (!neighbour.isVisited())
				{
					int newDist = workingNode.getDistance(origin) + workingNode.getImmediateDistance(neighbour);
					if (newDist < neighbour.getDistance(origin))
					{
						queue.remove(neighbour);
						neighbour.setDistance(origin, newDist);
						neighbour.setPredecessor(origin, workingNode);
						queue.add(neighbour);
					}
				}
			}
			workingNode.setVisited(true);
		}
		origin.setHasCalculated(true);
	}
	
	// Used by the Retriever to find the closest inventory that matches its filter
	public Pair<PipeNode, Direction> findSuitableSource(PipeNode origin, List<ItemStack> filter)
	{
		if (!origin.hasCalculated()) pathNodalDijkstra(origin); // Calculate paths from the origin
		// Make a queue of all nodes and sort them from closest to furthest
		PriorityQueue<PipeNode> queue = new PriorityQueue<>((n1, n2) -> PipeNetwork.compareDistance(n1, n2, origin));
		queue.addAll(allNodes.inverse().keySet());
		// Then iterate through them until one is found that matches the given filter
		for (PipeNode node : queue)
		{
			for (Direction f : node.getInventoryDirections())
			{
				NetworkedInventory inv = node.getInventory(f);
				if (InventoryHelper.isInventorySuitable(inv.inventory, filter))
				{
					return new Pair<>(node, f);
				}
			}
		}
		return null;
	}
	
	// Used by network-inserting tiles to find the closest suitable destination for the ItemStack.
	public Pair<PipeNode, Direction> findSuitableDestination(PipeNode origin, ItemStack is, boolean allowBackstuff)
	{
		if (!origin.hasCalculated()) 
		{
			pathNodalDijkstra(origin);
		}
		LinkedList<PipeNode> queue = new LinkedList<>();
		queue.addAll(allNodes.inverse().keySet()); // allNodes, sorted in order from closest to furthest
		queue.sort((n1, n2) -> n1.getDistance(origin) - n2.getDistance(origin));
		Logger.info("Nodes in network: " + queue);
		for (PipeNode node : queue)
		{
			NetworkedInventory inv = node.getValidInventory(is);
			if (inv != null)
			{
				Logger.info("Found suitable inventory connected to " + node);
				Direction f = inv.getDirectionFromNode(node);
				return new Pair<>(node, f);
			}
		}
		
		if (allowBackstuff)
		{
			for (PipeNode node : queue)
			{
				NetworkedInventory inv = node.getBackstuffInventory(is);
				if (inv != null)
				{
					Direction f = inv.getDirectionFromNode(node);
					return new Pair<>(node, f);
				}
			}
		}
		return null; // or origin?
	}
	
	public LinkedList<PipeNode> pathNodal(PipeNode origin, PipeNode dest)
	{
//		Logger.info("Trying to path from " + origin + " to " + dest);
		if (!(origin.hasCalculated() /* && dest.hasCalculated(colour) */)) pathNodalDijkstra(origin);
		LinkedList<PipeNode> steps = new LinkedList<>();
		for (PipeNode node = dest; node != origin; node = node.getPredecessor(origin))
		{
			steps.addFirst(node);
		}
//		steps.addFirst(origin);
//		Logger.info(steps);
		return steps;
	}
	
	@Override
	public String toString()
	{
		return "NETWORK with id " + id;
	}

	public void markNodeDirty(PipeNode pipeNode)
	{
		allItems.forEach(item -> item.markDirtyIfContains(pipeNode));
	}
}
