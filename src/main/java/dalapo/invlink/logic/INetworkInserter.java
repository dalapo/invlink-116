package dalapo.invlink.logic;

import net.minecraft.util.math.BlockPos;

public interface INetworkInserter
{
	public BlockPos getPos();
}