package dalapo.invlink.logic;


import net.minecraft.util.math.Direction;

public interface INetworkedObject
{
	public PipeNetwork getNetwork();
	public void setNetwork(PipeNetwork net);
	public boolean canConnectToSide(Direction side);
}