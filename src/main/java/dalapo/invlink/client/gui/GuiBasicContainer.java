package dalapo.invlink.client.gui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import dalapo.invlink.client.gui.widget.InvLinkWidget;
import dalapo.invlink.helper.GuiHelper;
import dalapo.invlink.lookup.NameList;
import dalapo.invlink.tile.TileEntityBase;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;

// ScreenHandler: Container
// HandledScreen: GUI
public class GuiBasicContainer<T extends ScreenHandler> extends HandledScreen<T>
{
	protected Inventory playerInv;
	protected String texture;
	private List<InvLinkWidget> widgets = new ArrayList<>();
	protected BlockPos pos;
	
	public GuiBasicContainer(T handler, PlayerInventory playerInv, Text title)
	{
		super(handler, playerInv, title);
		pos = ((ContainerBase)handler).getPos();
	}
	
	public GuiBasicContainer(String texName, String title, T inventorySlotsIn, int xSize, int ySize, PlayerInventory playerInv)
	{
		super(inventorySlotsIn, playerInv, Text.of(title));
		this.playerInv = playerInv;
		this.width = xSize;
		this.height = ySize;
		this.texture = texName;
	}
	
	@Override
	public void init()
	{
		super.init();
		widgets.forEach(widget -> widget.init());
	}

	public GuiBasicContainer<T> addWidget(InvLinkWidget widget)
	{
		widgets.add(widget);
		return this;
	}
	
    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta)
    {
        renderBackground(matrices);
        super.render(matrices, mouseX, mouseY, delta);
    }
	
	@Override
	protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY)
	{
		GuiHelper.bindTexture(texture);
		this.drawTexture(matrices, x, y, 0, 0, 256, 256);
		matrices.push();
		matrices.translate(x, y, 0);
		widgets.forEach(widget -> widget.render(matrices, mouseX, mouseY, delta));
        matrices.pop();
        drawMouseoverTooltip(matrices, mouseX, mouseY);
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int mouseButton)
	{
		super.mouseClicked(mouseX, mouseY, mouseButton);
		for (InvLinkWidget widget : widgets)
		{
			if (widget.isPointInBounds(mouseX - x, mouseY - y))
			{
				widget.handle(mouseX - x, mouseY - y, mouseButton, Screen.hasShiftDown());
				return true;
			}
		}
		return false;
	}
	
	@Override
	public void drawMouseoverTooltip(MatrixStack matrices, int mouseX, int mouseY)
	{
		super.drawMouseoverTooltip(matrices, mouseX, mouseY);
		widgets.forEach(widget -> {
			if (widget.isPointInBounds(mouseX - x, mouseY - y))
			{
				String tooltip = widget.getTooltip();
				this.renderTooltip(matrices, Text.of(tooltip), mouseX, mouseY);
			}
		});
	}
}