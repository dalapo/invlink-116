package dalapo.invlink.client.gui;

import dalapo.invlink.lookup.GuiRegistry;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.slot.Slot;

public class ContainerInventory extends ContainerBase
{
	public ContainerInventory(ScreenHandlerType<? extends ContainerBase> type, int syncID, int rows, int cols, PlayerInventory player, Inventory tile, PropertyDelegate del)
	{
		super(type, syncID, rows, cols, player, (Inventory)tile, del);
	}
	
	public ContainerInventory(ScreenHandlerType<? extends ContainerBase> type, int syncID, int rows, int cols, PlayerInventory player, PacketByteBuf buffer, PropertyDelegate del)
	{
		super(type, syncID, rows, cols, player, buffer, del);
	}
}