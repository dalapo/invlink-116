package dalapo.invlink.client.gui;

import dalapo.invlink.helper.InventoryHelper;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.slot.SlotActionType;

public class GhostSlot extends SlotCustom
{
	public GhostSlot(Inventory inventory, int index, int xPosition, int yPosition, ContainerBase parent)
	{
		super(inventory, index, xPosition, yPosition, parent);
	}

	@Override
	public ItemStack handleClick(int dragType, SlotActionType clickTypeIn, PlayerEntity player, ItemStack holding)
	{
		System.out.println(String.format("%s, %s", dragType, clickTypeIn.toString()));
		ItemStack is = inventory.getStack(id);
		if (dragType == 0) // Left click
		{
			switch(clickTypeIn)
			{
			case PICKUP:
			case PICKUP_ALL:
				if (InventoryHelper.canStacksCombine(is, holding) && !is.isEmpty() && !holding.isEmpty())
				{
					is.increment(holding.getCount());
				}
				else
				{
					inventory.setStack(id, holding.copy());
				}
				break;
			case QUICK_MOVE:
				inventory.setStack(id, ItemStack.EMPTY);
				default:
					// Do nothing
			}
		}
		else if (dragType == 1) // Right click
		{
			switch (clickTypeIn)
			{
			case PICKUP:
			case PICKUP_ALL:
				if (is.isEmpty())
				{
					ItemStack toInsert = holding.copy();
					toInsert.setCount(1);
					inventory.setStack(id, toInsert);
				}
				else
				{
					is.increment(1);
				}
				break;
			case QUICK_MOVE:
				if (!is.isEmpty()) is.decrement(1);
				break;
				default:
					// Do nothing
			}
		}
		return holding;
	}
}