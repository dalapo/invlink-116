package dalapo.invlink.client.gui;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;

public abstract class SlotCustom extends Slot
{
	protected Inventory handler;
	protected ContainerBase parent;
	
	public SlotCustom(Inventory itemHandler, int index, int xPosition, int yPosition, ContainerBase parent)
	{
		super(itemHandler, index, xPosition, yPosition);
		this.parent = parent;
		this.handler = itemHandler;
	}
	
	public abstract ItemStack handleClick(int dragType, SlotActionType clickTypeIn, PlayerEntity player, ItemStack holding);
}