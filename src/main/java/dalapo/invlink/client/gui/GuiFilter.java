package dalapo.invlink.client.gui;

import dalapo.invlink.tile.TileEntityBase;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;

public class GuiFilter extends GuiBasicContainer<ContainerFilter>
{
	public GuiFilter(ContainerFilter inventorySlotsIn, int xSize, int ySize, PlayerInventory playerInv)
	{
		super("filter", "Filter", inventorySlotsIn, xSize, ySize, playerInv);
	}
}