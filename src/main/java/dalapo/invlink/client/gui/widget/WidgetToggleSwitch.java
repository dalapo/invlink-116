package dalapo.invlink.client.gui.widget;

import dalapo.invlink.client.gui.ContainerBase;
import dalapo.invlink.client.gui.GuiBasicContainer;
import dalapo.invlink.client.gui.GuiPropertyDelegate;
import dalapo.invlink.helper.GuiHelper;
import dalapo.invlink.packet.PacketHandler;
import dalapo.invlink.packet.PacketToggleField;
import net.minecraft.client.gui.DrawableHelper;
import net.minecraft.client.util.math.MatrixStack;

public class WidgetToggleSwitch extends InvLinkWidget
{
	private int id;
//	private boolean state;
	private String offMessage;
	private String onMessage;
	
	public WidgetToggleSwitch(GuiPropertyDelegate parent, int id, int x, int y, String off, String on)
	{
		super(parent, x, y, 18, 10);
		this.id = id;
		onMessage = on;
		offMessage = off;
	}

	public void init()
	{
//		state = parent.getDelegate().get(id) != 0;
	}
	
	@Override
	public void handle(double mouseX, double mouseY, int mouseButton, boolean shift)
	{
//		state = !state;
		GuiBasicContainer<?> p = (GuiBasicContainer<?>)parent;
		PacketToggleField packet = new PacketToggleField(((ContainerBase)p.getScreenHandler()).getPos(), 0);
		PacketHandler.sendToServer(packet);
	}

	@Override
	public String getTooltip()
	{
		return parent.getDelegate().get(id) != 0 ? onMessage : offMessage;
//		return state ? onMessage : offMessage;
	}

	@Override
	public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) 
	{
		boolean state = parent.getDelegate().get(id) != 0;
		matrices.push();
		GuiHelper.bindTexture("widgets");
		DrawableHelper.drawTexture(matrices, x, y, 0, 0, state ? 10 : 0, width, height, 256, 256);
		matrices.pop();
	}
}