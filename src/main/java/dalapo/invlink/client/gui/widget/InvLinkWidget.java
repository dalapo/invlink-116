package dalapo.invlink.client.gui.widget;

import dalapo.invlink.client.gui.GuiBasicContainer;
import dalapo.invlink.client.gui.GuiPropertyDelegate;
import net.minecraft.client.gui.Drawable;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.util.math.MatrixStack;

public abstract class InvLinkWidget implements Drawable
{
	protected int x;
	protected int y;
	protected int width;
	protected int height;
	protected GuiPropertyDelegate parent;
	
	public InvLinkWidget(GuiPropertyDelegate p, int x, int y, int w, int h)
	{
		this.x = x;
		this.y = y;
		this.width = w;
		this.height = h;
		this.parent = p;
	}
	
	public abstract void init();
	public abstract void handle(double mouseX, double mouseY, int mouseButton, boolean shift);
	
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public int getWidth()
	{
		return width;
	}
	public int getHeight()
	{
		return height;
	}
	
	protected GuiPropertyDelegate getParent()
	{
		return parent;
	}
	
	public boolean isPointInBounds(double x, double y)
	{
		return x >= this.x && x <= this.x + this.width && y >= this.y && y <= this.y + this.height;
	}
	public abstract String getTooltip();
}