package dalapo.invlink.client.gui;

import dalapo.invlink.client.gui.widget.WidgetToggleSwitch;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerListener;
import net.minecraft.text.Text;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;

public class GuiRouter extends GuiBasicContainer<ContainerInventory> implements GuiPropertyDelegate
{
	public GuiRouter(ContainerInventory handler, PlayerInventory playerInv, Text title)
	{
		super(handler, playerInv, title);
	}
	
	public GuiRouter(String texName, String title, ContainerInventory inventorySlotsIn, int xSize, int ySize, PlayerInventory playerInv)
	{
		super(texName, title, inventorySlotsIn, xSize, ySize, playerInv);
		addWidget(new WidgetToggleSwitch(this, 0, 20, 20, "Insert", "Extract"));
	}

	@Override
	public PropertyDelegate getDelegate()
	{
		return ((ContainerBase)getScreenHandler()).getDelegate();
	}
}