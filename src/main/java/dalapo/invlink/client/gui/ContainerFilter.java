package dalapo.invlink.client.gui;

import dalapo.invlink.lookup.GuiRegistry;
import dalapo.invlink.tile.TileEntityBase;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.slot.Slot;

public class ContainerFilter extends ContainerBase
{	
	public ContainerFilter(int syncID, PlayerInventory player, PacketByteBuf buffer, PropertyDelegate del)
	{
		super(GuiRegistry.FILTER_CONTAINER, syncID, 3, 3, player, buffer, del);
	}
	
	public ContainerFilter(int syncID, PlayerInventory player, Inventory tile, PropertyDelegate del)
	{
		super(GuiRegistry.FILTER_CONTAINER, syncID, 3, 3, player, tile, del);
		
	}
	
	@Override
	public boolean canUse(PlayerEntity playerIn)
	{
		return true;
	}
	
	@Override
	public ItemStack transferSlot(PlayerEntity ep, int index)
	{
//		Thread.dumpStack();
		return ItemStack.EMPTY; // Basically do nothing
	}
	
	@Override
	protected void fillSlots(int rows, int cols, PlayerInventory player)
	{
		int slot = 0;
		for (int row=0; row<rows; row++)
		{
			for (int col=0; col<cols; col++, slot++)
			{
				this.addSlot(new GhostSlot(getTile(), slot, (89 - (9 * cols)) + (col * 18), (71 - (18 * rows)) + (row * 18), this));
			}
		}
		
		if (player != null)
		{
			for (int y = 0; y < 3; ++y)
			{
		        for (int x = 0; x < 9; ++x, slot++)
		        {
		            this.addSlot(new Slot(player, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
		        }
		    }
			
			// Player hotbar
			for (int i=0; i<9; i++)
			{
				this.addSlot(new Slot(player, i, 8 + i * 18, 142));
			}
		}
	}
}