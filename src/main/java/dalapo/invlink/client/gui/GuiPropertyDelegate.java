package dalapo.invlink.client.gui;

import dalapo.invlink.tile.TileEntityBase;
import net.minecraft.screen.PropertyDelegate;

public interface GuiPropertyDelegate
{
	public PropertyDelegate getDelegate();
}
