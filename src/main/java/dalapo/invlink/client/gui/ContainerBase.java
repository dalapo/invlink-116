package dalapo.invlink.client.gui;


import dalapo.invlink.auxiliary.EmptyPropertyDelegate;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.lookup.GuiRegistry;
import dalapo.invlink.tile.TileEntityBase;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.screen.slot.Slot;
import net.minecraft.screen.slot.SlotActionType;
import net.minecraft.util.math.BlockPos;

public abstract class ContainerBase extends ScreenHandler
{
	private Inventory tile;
	private PropertyDelegate delegate;
	private BlockPos pos;
	
	public ContainerBase(ScreenHandlerType<? extends ContainerBase> id, int syncID, int rows, int cols, PlayerInventory player, Inventory te, PropertyDelegate del)
	{
		super(id, syncID);
		tile = te;
		pos = BlockPos.ORIGIN;
		delegate = del;
		if (del != null) this.addProperties(del);
		checkSize(te, rows * cols);
		fillSlots(rows, cols, player);
	}
	
	public ContainerBase(ScreenHandlerType<? extends ContainerBase> id, int syncID, int rows, int cols, PlayerInventory player, PacketByteBuf buffer, PropertyDelegate del)
	{
		this(id, syncID, rows, cols, player, new SimpleInventory(rows * cols), del);
		pos = buffer.readBlockPos();
	}
	
	protected PropertyDelegate getDelegate()
	{
		if (delegate == null) throw new NullPointerException("Called getDelegate() but it does not exist!");
		return delegate;
	}
	
	@Override
	public boolean canUse(PlayerEntity playerIn)
	{
		return true;
	}
	
	public BlockPos getPos()
	{
		return pos;
	}
	
	@Override
	public ItemStack onSlotClick(int slotId, int dragType, SlotActionType clickTypeIn, PlayerEntity player)
	{
		if (InvLinkMathHelper.isInRange(slotId, 0, slots.size()) && slots.get(slotId) instanceof SlotCustom)
		{
			return ((SlotCustom)slots.get(slotId)).handleClick(dragType, clickTypeIn, player, player.inventory.getCursorStack());
		}
		else
		{
			return super.onSlotClick(slotId, dragType, clickTypeIn, player);
		}
	}
	
	public Inventory getTile()
	{
		return tile;
	}
	
	protected void fillSlots(int rows, int cols, PlayerInventory player)
	{
		int slot = 0;
		for (int row=0; row<rows; row++)
		{
			for (int col=0; col<cols; col++, slot++)
			{
				this.addSlot(new Slot(tile, slot, (89 - (9 * cols)) + (col * 18), (53 - (18 * rows)) + (row * 18)));
			}
		}
		
		if (player != null)
		{
			for (int y = 0; y < 3; ++y)
			{
		        for (int x = 0; x < 9; ++x, slot++)
		        {
		            this.addSlot(new Slot(player, x + y * 9 + 9, 8 + x * 18, 84 + y * 18));
		        }
		    }
			
			// Player hotbar
			for (int i=0; i<9; i++)
			{
				this.addSlot(new Slot(player, i, 8 + i * 18, 142));
			}
		}
	}
}