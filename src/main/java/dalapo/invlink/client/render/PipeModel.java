package dalapo.invlink.client.render;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import com.mojang.datafixers.util.Pair;

import dalapo.invlink.lookup.MaterialRegistry;
import net.fabricmc.fabric.api.renderer.v1.Renderer;
import net.fabricmc.fabric.api.renderer.v1.RendererAccess;
import net.fabricmc.fabric.api.renderer.v1.mesh.Mesh;
import net.fabricmc.fabric.api.renderer.v1.mesh.MeshBuilder;
import net.fabricmc.fabric.api.renderer.v1.mesh.MutableQuadView;
import net.fabricmc.fabric.api.renderer.v1.mesh.QuadEmitter;
import net.fabricmc.fabric.api.renderer.v1.model.FabricBakedModel;
import net.fabricmc.fabric.api.renderer.v1.render.RenderContext;
import net.minecraft.block.BlockState;
import net.minecraft.client.render.VertexFormat;
import net.minecraft.client.render.model.BakedModel;
import net.minecraft.client.render.model.BakedQuad;
import net.minecraft.client.render.model.ModelBakeSettings;
import net.minecraft.client.render.model.ModelLoader;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.client.render.model.json.ModelOverrideList;
import net.minecraft.client.render.model.json.ModelTransformation;
import net.minecraft.client.texture.Sprite;
import net.minecraft.client.texture.SpriteAtlasTexture;
import net.minecraft.client.util.SpriteIdentifier;
import net.minecraft.client.util.math.Vector3f;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.BlockRenderView;

public class PipeModel implements BakedModel, UnbakedModel, FabricBakedModel
{
	private static final Sprite[] sprites = new Sprite[1];
	private static final SpriteIdentifier[] spriteIDs = new SpriteIdentifier[] {
			new SpriteIdentifier(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE, new Identifier("invlink:block/pipe"))
	};
	private Mesh mesh;
	private VertexFormat format;
	
	@Override
	public boolean isVanillaAdapter()
	{
		return false;
	}

	@Override
	public void emitBlockQuads(BlockRenderView blockView, BlockState state, BlockPos pos, Supplier<Random> randomSupplier, RenderContext context)
	{
		context.meshConsumer().accept(mesh);
	}

	@Override
	public void emitItemQuads(ItemStack stack, Supplier<Random> randomSupplier, RenderContext context)
	{
		
	}

	@Override
	public Collection<Identifier> getModelDependencies()
	{
		return Collections.emptyList();
	}

	@Override
	public Collection<SpriteIdentifier> getTextureDependencies(Function<Identifier, UnbakedModel> unbakedModelGetter, Set<Pair<String, String>> unresolvedTextureReferences)
	{
		return Arrays.asList(spriteIDs);
	}
	
	private void createRect(QuadEmitter emitter, Direction normal, float offset, float ax, float ay, float bx, float by)
	{
		emitter.square(normal, ax, ay, bx, by, offset);
		emitter.sprite(0, 0, 0, 0);
		emitter.sprite(1, 0, 0, 16);
		emitter.sprite(2, 0, 16, 16);
		emitter.sprite(3, 0, 16, 0);
		emitter.spriteBake(0, sprites[0], MutableQuadView.BAKE_ROTATE_NONE);
		emitter.spriteColor(0, -1, -1, -1, -1);
		emitter.emit();
	}
	
	@Override
	public BakedModel bake(ModelLoader loader, Function<SpriteIdentifier, Sprite> textureGetter, ModelBakeSettings rotationContainer, Identifier modelId)
	{
		for (int i=0; i<sprites.length; i++)
		{
			sprites[i] = textureGetter.apply(spriteIDs[i]);
		}
		Renderer renderer = RendererAccess.INSTANCE.getRenderer();
		MeshBuilder builder = renderer.meshBuilder();
		QuadEmitter emitter = builder.getEmitter();
//		for (Direction dir : Direction.values())
		for (int i=0; i<Direction.values().length; i++)
		{
			Direction dir = Direction.byId(i);
			createRect(emitter, dir, 0.25f, 0.25f, 0.25f, 0.75f, 0.75f);
		}
		mesh = builder.build();
		return this;
	}

	@Override
	public List<BakedQuad> getQuads(BlockState state, Direction face, Random random)
	{
		return null;
	}

	@Override
	public boolean useAmbientOcclusion() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean hasDepth() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isSideLit() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isBuiltin() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Sprite getSprite() {
		// TODO Auto-generated method stub
		return sprites[0];
	}

	@Override
	public ModelTransformation getTransformation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ModelOverrideList getOverrides() {
		// TODO Auto-generated method stub
		return null;
	}

}