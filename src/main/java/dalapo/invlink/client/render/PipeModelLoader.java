package dalapo.invlink.client.render;

import net.fabricmc.fabric.api.client.model.ModelProviderContext;
import net.fabricmc.fabric.api.client.model.ModelProviderException;
import net.fabricmc.fabric.api.client.model.ModelResourceProvider;
import net.minecraft.client.render.model.UnbakedModel;
import net.minecraft.util.Identifier;

public class PipeModelLoader implements ModelResourceProvider
{
	private static final Identifier PIPE_MODEL = new Identifier("invlink", "block/pipe");
	@Override
	public UnbakedModel loadModelResource(Identifier resourceId, ModelProviderContext context) throws ModelProviderException
	{
		if (resourceId.equals(PIPE_MODEL)) return new PipeModel();
		return null;
	}
}