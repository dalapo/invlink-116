package dalapo.invlink.client.render.tesr;

import org.lwjgl.opengl.GL11;

import com.mojang.blaze3d.platform.GlStateManager;

import dalapo.invlink.helper.RenderHelper;
import dalapo.invlink.logic.RoutedItem;
import dalapo.invlink.logic.RoutedItemClient;
import dalapo.invlink.tile.TileEntityItemPipe;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.math.Direction;


public class TesrItemPipe extends BlockEntityRenderer<TileEntityItemPipe>
{
	public TesrItemPipe(BlockEntityRenderDispatcher dispatcher)
	{
		super(dispatcher);
	}
	
	@Override
	public void render(TileEntityItemPipe te, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay)
	{
		for (RoutedItemClient ri : te.getClientItems())
		{
			matrices.push();
			double d = 0.5 / RoutedItem.TICKS;
			Direction dir = ri.age < 0 ? ri.entryDir : ri.exitDir;
			matrices.translate(0.5, 0.375, 0.5);
			if (dir != null) matrices.translate(dir.getOffsetX()*ri.age*d, dir.getOffsetY()*ri.age*d, dir.getOffsetZ()*ri.age*d);
			RenderHelper.renderItemStack(ri.is, matrices, vertexConsumers, light, overlay);
			matrices.pop();
		}
	}
}