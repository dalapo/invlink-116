package dalapo.invlink.item;

import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.helper.ChatHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.logic.PipeNode;
import dalapo.invlink.packet.PacketHandler;
import dalapo.invlink.packet.PacketSendChatInfo;
import dalapo.invlink.tile.TileEntityItemPipe;
import dalapo.invlink.tile.TileEntityRouter;
import dalapo.invlink.tile.TileEntityTeleportPipe;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

public class ItemNetworkDebugger extends ItemBase
{
	public ItemNetworkDebugger(Settings settings, String name)
	{
		super(settings, name);
	}
	
	@Override
	public ActionResult useOnBlock(ItemUsageContext context)
	{
		World world = context.getWorld();
		BlockPos pos = context.getBlockPos();
		BlockEntity te = world.getBlockEntity(pos);
		if (te instanceof TileEntityItemPipe && !world.isClient)
		{
			TileEntityItemPipe pipe = (TileEntityItemPipe)te;
			String info = "Network: " + pipe.getNetwork().id + "; ";
			if (pipe.isNode())
			{
				info += (pipe.getNetwork().findNode(pipe).toString());
			}
			else if (pipe.isEdge())
			{
				Pair<PipeNode, PipeNode> edge = pipe.getNetwork().getEdge(pipe);
				info += "EDGE: " + edge.toString();
			}
			else if (pipe.isDeadEnd())
			{
				info += "DEAD END: Connected to " + pipe.getNodes().a;
			}
			Logger.info(info);
//			PacketHandler.sendToPlayer(new PacketSendChatInfo(info), ep);
			return ActionResult.SUCCESS;
		}
		else if (te instanceof TileEntityTeleportPipe && !world.isClient)
		{
			TileEntityTeleportPipe tele = (TileEntityTeleportPipe)te;
			if (tele.getLink() != null) Logger.info(String.format("TELEPORT linked to %s", tele.getLink().getPos()));
			else Logger.info("TELEPORT, unlinked");
		}
		else if (te instanceof TileEntityRouter && !world.isClient)
		{
			TileEntityRouter router = (TileEntityRouter)te;
			Logger.info(String.format("ROUTER, connected to %s tiles", router.getConnectedSize()));
		}
		return ActionResult.PASS;
	}
}