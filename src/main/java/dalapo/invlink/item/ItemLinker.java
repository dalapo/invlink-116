package dalapo.invlink.item;

import dalapo.invlink.InvLinkConfigManager;
import dalapo.invlink.auxiliary.ILinkable;
import dalapo.invlink.helper.ChatHelper;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.Logger;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemLinker extends ItemBase
{
	public ItemLinker(Settings settings, String name)
	{
		super(settings, name);
	}
	
	@Override
	public ActionResult useOnBlock(ItemUsageContext context)
	{
		super.useOnBlock(context);
		Logger.info("Calling useOnBlock");
		World world = context.getWorld();
		Logger.info(world.isClient);
		BlockPos pos = context.getBlockPos();
		Hand hand = context.getHand();
		PlayerEntity ep = context.getPlayer();
		BlockEntity te = world.getBlockEntity(pos);
		if (te instanceof ILinkable)
		{
			ItemStack is = ep.getStackInHand(hand);
			if (!is.hasTag())
			{
				ChatHelper.sendChatToPlayer(ep, String.format("Stored position (%s, %s, %s)", pos.getX(), pos.getY(), pos.getZ()));
				CompoundTag tag = new CompoundTag();
				tag.putLong("pos", pos.asLong());
				is.setTag(tag);
			}
			else
			{
				BlockPos storedPos = BlockPos.fromLong(is.getTag().getLong("pos"));
				if (((ILinkable)te).link(storedPos, ep)) is.setTag(null);
			}
			return ActionResult.SUCCESS;
		}
		return ActionResult.PASS;
	}
}
