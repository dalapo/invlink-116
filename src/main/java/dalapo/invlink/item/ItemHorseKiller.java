package dalapo.invlink.item;

import java.util.function.Consumer;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.damage.EntityDamageSource;
import net.minecraft.entity.passive.HorseEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SwordItem;
import net.minecraft.item.ToolMaterial;

/**
 * Change my mind: horses should never have been added to Minecraft.
 *
 */
public class ItemHorseKiller extends ItemBase
{
	
	public ItemHorseKiller(Settings settings, String name)
	{
		super(settings, name);
	}

	public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
	      if (target instanceof HorseEntity && attacker instanceof PlayerEntity)
	      {
	    	  target.damage(new EntityDamageSource("removed", attacker), Float.MAX_VALUE);
	    	  return true;
	      }
	      return false;
	}
}