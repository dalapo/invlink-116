package dalapo.invlink.item;

import java.util.Set;

import dalapo.invlink.logic.NetworkedInventory;
import dalapo.invlink.logic.PipeNode;
import dalapo.invlink.tile.TileEntityItemPipe;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class ItemNodeDebugger extends ItemBase
{
	public ItemNodeDebugger(Settings settings, String name)
	{
		super(settings, name);
	}
	
	@Override
	public ActionResult useOnBlock(ItemUsageContext ctx)
	{
		World world = ctx.getWorld();
		BlockPos pos = ctx.getBlockPos();
		BlockEntity te = world.getBlockEntity(pos);
		PlayerEntity ep = ctx.getPlayer();
		if (te instanceof TileEntityItemPipe && !world.isClient)
		{
			TileEntityItemPipe pipe = (TileEntityItemPipe)te;
			if (pipe.isNode())
			{
				System.out.println("Network: " + pipe.getNetwork().id + "\n");
				PipeNode p = pipe.getNodes().a;
				if (ep.isSneaking()) p.calcConnections();
				System.out.println(p.toString() + "\n");
				System.out.println("NEIGHBOURS:");
				p.getNeighbours().forEach(neighbour -> System.out.println(String.format("%s, distance %s, immediately connected to %s", neighbour, p.getImmediateDistance(neighbour), p.getImmediateConnection(neighbour).getPos())));
				System.out.println("INVENTORIES:");
				Set<NetworkedInventory> inventories = p.getNetwork().getConnectedInventories(p);
				inventories.forEach(inv -> System.out.println(inv.toString()));
			}
			return ActionResult.SUCCESS;
		}
		return ActionResult.PASS;
	}
}