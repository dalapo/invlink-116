package dalapo.invlink.item;

import dalapo.invlink.lookup.ItemRegistry;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;

public class ItemBase extends Item
{
	private final Identifier id;
	public ItemBase(Settings settings, String name)
	{
		super(settings);
		id = new Identifier("invlink", name);
		ItemRegistry.addItem(this);
	}
	
	public Identifier getID()
	{
		return id;
	}
}
