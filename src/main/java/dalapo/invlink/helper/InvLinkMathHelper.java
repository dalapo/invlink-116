package dalapo.invlink.helper;

import dalapo.invlink.logic.PipeNode;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;

public class InvLinkMathHelper
{
	private InvLinkMathHelper() {}
	
	/**
	 * Gets relative direction of block b to block a
	 * @param a
	 * @param b
	 * @return
	 */
	public static Direction getRelativeDirection(BlockPos a, BlockPos b)
	{
		Vec3i vec = b.subtract(a);
		return Direction.fromVector(vec.getX(), vec.getY(), vec.getZ());
	}
	
	/**
	 * Returns true if <code>x >= min && x < max </code>
	 * @param x
	 * @param min
	 * @param max
	 * @return
	 */
	public static boolean isInRange(int x, int min, int max)
	{
		return (x >= min && x < max);
	}
	
	/**
	 * Returns true if <code>x >= min && x <= max </code>
	 * @param x
	 * @param min
	 * @param max
	 * @return
	 */
	public static boolean isInRangeIncl(int x, int min, int max)
	{
		return (x >= min && x <= max);
	}
	
	public static double getAbsoluteDistance(BlockPos a, BlockPos b)
	{
		return Math.sqrt(a.getSquaredDistance(b));
	}
}