package dalapo.invlink.helper;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.BufferBuilder;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.model.json.ModelTransformation.Mode;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.ItemStack;

public class RenderHelper
{
	private RenderHelper() {}
	
	public static void renderItemStack(ItemStack is, MatrixStack matrices, VertexConsumerProvider consumers, int light, int overlay)
	{
		MinecraftClient.getInstance().getItemRenderer().renderItem(is, Mode.GROUND, light, overlay, matrices, consumers);
	}
	
	public static void drawLine(BufferBuilder builder, Point a, Point b)
	{
		builder.vertex(a.x, a.y, a.z).vertex(b.x, b.y, b.z);
	}
	
	public static class Point
	{
		double x;
		double y;
		double z;
		
		public Point(double i, double j, double k)
		{
			x = i;
			y = j;
			z = k;
		}
		
		public void scale(double i, double j, double k)
		{
			x *= i;
			y *= j;
			z *= k;
		}
		
		public double getField(int i)
		{
			switch (i)
			{
			case 0:
				return x;
			case 1:
				return y;
			case 2:
				return z;
				default:
					return 0.0;
			}
		}
	}
}