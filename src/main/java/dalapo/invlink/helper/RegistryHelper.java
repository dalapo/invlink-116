package dalapo.invlink.helper;

import dalapo.invlink.block.BlockBase;
import dalapo.invlink.item.ItemBase;
import dalapo.invlink.tile.TileEntityBase;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.registry.Registry;

public class RegistryHelper
{
	private RegistryHelper() {}
	
	public static void registerBlock(BlockBase b)
	{
		Registry.register(Registry.BLOCK, b.getID(), b);
		Registry.register(Registry.ITEM, b.getID(), new BlockItem(b, new Item.Settings().group(ItemGroup.TRANSPORTATION)));
	}
	
	public static void registerItem(ItemBase i)
	{
		Registry.register(Registry.ITEM, i.getID(), i);
	}
}