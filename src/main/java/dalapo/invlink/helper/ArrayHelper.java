package dalapo.invlink.helper;

public class ArrayHelper
{
	private ArrayHelper() {}
	
	public static final int[] EMPTY_ARRAY = new int[0];
	
	/**
	 * Returns an array of the given size where each index's member is equal to the index
	 * @param size
	 * @return
	 */
	public static int[] getFilled(int size)
	{
		int[] filled = new int[size];
		for (int i=0; i<size; i++)
		{
			filled[i] = i;
		}
		return filled;
	}
}