package dalapo.invlink.helper;

import dalapo.invlink.lookup.NameList;
import net.fabricmc.api.Environment;
import net.fabricmc.api.EnvType;
import net.minecraft.client.MinecraftClient;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class GuiHelper
{
	private GuiHelper() {}
	
	public static Identifier formatTexLocation(String texName)
	{
		return new Identifier(NameList.MOD_ID + ":textures/gui/" + texName + ".png");
	}
	
	public static void bindTexture(String tex)
	{
		MinecraftClient.getInstance().getTextureManager().bindTexture(formatTexLocation(tex));
	}
	
//	public static void renderTooltip(GuiScreen parent, int x, int y, String tooltip)
//	{
//		parent.drawHoveringText(tooltip, x, y);
//	}
}