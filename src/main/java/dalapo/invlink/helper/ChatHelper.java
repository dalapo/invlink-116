package dalapo.invlink.helper;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;

public class ChatHelper
{
	@SuppressWarnings("resource")
	public static void sendChatToPlayer(PlayerEntity ep, String msg)
	{
		if (ep.getEntityWorld().isClient)
		{
			String[] parts = msg.split("\\n");
			for (int i=0; i<parts.length; i++)
			{
				Text chatPart = new LiteralText(parts[i]);
				ep.sendMessage(chatPart, false);
			}
		}
	}
}