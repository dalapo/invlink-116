package dalapo.invlink.helper;

import org.apache.logging.log4j.Level;

import dalapo.invlink.InventoryLink;

public class Logger
{
	private Logger() {}
	
	public static void info(Object o)
	{
		InventoryLink.logger.log(Level.INFO, o.toString());
	}
	
	public static void debug(Object o)
	{
		InventoryLink.logger.log(Level.DEBUG, o.toString());
	}
}