package dalapo.invlink.helper;

import java.util.LinkedList;

import dalapo.invlink.logic.PipeNode;
import net.minecraft.util.math.Direction;

public class NetworkHelper
{
	private NetworkHelper() {}
	
	public static LinkedList<Direction> convertNodeList(LinkedList<PipeNode> in)
	{
		LinkedList<Direction> list = new LinkedList<>();
		for (int i=1; i<in.size(); i++)
		{
			list.add(InvLinkMathHelper.getRelativeDirection(in.get(i-1).getTile().getPos(), in.get(i-1).getImmediateConnection(in.get(i)).getPos()));
		}
		list.add(null);
		return list;
	}
}