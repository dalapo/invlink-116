package dalapo.invlink.helper;

import dalapo.invlink.lookup.StateList;
import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

public class BlockHelper
{
	private BlockHelper() {}
	
	public static void rotateBlock(World world, BlockPos pos)
	{
		if (world.getBlockState(pos).contains(StateList.directions))
		{
			Direction newDir = Direction.byId((world.getBlockState(pos).get(StateList.directions).getId() + 1) % 6);
			world.setBlockState(pos, world.getBlockState(pos).with(StateList.directions, newDir));
		}
	}
	
	public static void updateBlock(World world, BlockPos pos)
	{
		BlockState state = world.getBlockState(pos);
		world.onBlockChanged(pos, state, state);
	}
	
	public static Direction getBlockDirection(World world, BlockPos pos)
	{
		return world.getBlockState(pos).get(StateList.directions);
	}
}