package dalapo.invlink.helper;

import java.util.List;
import java.util.function.Predicate;

import dalapo.invlink.auxiliary.BackstuffInventory;
import dalapo.invlink.auxiliary.ItemHandler;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.Direction;

public class InventoryHelper
{
	public static ItemStack findFirst(Inventory inv, Predicate<ItemStack> predicate, Direction dir)
	{
		int[] slots;
		if (inv instanceof SidedInventory) slots = ((SidedInventory)inv).getAvailableSlots(dir);
		else slots = ArrayHelper.getFilled(inv.size());
		for (int slot : slots)
		{
			if (predicate.test(inv.getStack(slot))) return inv.getStack(slot);
		}
		return ItemStack.EMPTY;
	}
	
	public static ItemStack tryInsertItem(ItemHandler inv, ItemStack itemstack, boolean simulate)
	{
		if (inv == null) return itemstack;
//		Logger.info("Trying to insert " + itemstack + " into " + inv + " with " + inv.getSlots() + " slots");
		if (itemstack.isEmpty()) return ItemStack.EMPTY;
		for (int i=0; i<inv.getSlots(); i++)
		{
			itemstack = inv.insertItem(i, itemstack, simulate);
			if (itemstack.isEmpty()) break;
		}
		return itemstack;
	}
	
	/**
	 * Attempts to insert an ItemStack into an inventory from the given direction. Returns whatever could not be inserted.
	 * @param inv
	 * @param is
	 * @param dir
	 * @return
	 */
	public static ItemStack tryInsertItem(Inventory inv, ItemStack is, Direction dir, boolean simulate)
	{
		int[] slots;
		if (inv instanceof SidedInventory) slots = ((SidedInventory)inv).getAvailableSlots(dir);
		else slots = ArrayHelper.getFilled(inv.size());
		
		for (int s : slots)
		{
			if (!(inv instanceof SidedInventory) || ((SidedInventory)inv).canInsert(s, is, dir))
			{
				ItemStack stack = inv.getStack(s).copy();
				if (stack.isItemEqual(is))
				{
					int toTransfer = Math.min(is.getCount(), stack.getMaxCount() - is.getCount());
					stack.increment(toTransfer);
					is.decrement(toTransfer);
					inv.setStack(s, stack);
				}
			}
			if (is.isEmpty()) return ItemStack.EMPTY;
		}
		return is;
	}
	
	private static int getFilterSlot(ItemStack is, List<ItemStack> filter)
	{
		for (int i=0; i<filter.size(); i++)
		{
			if (is.isItemEqual(filter.get(i)) && is.getCount() >= filter.get(i).getCount()) return i;
		}
		return -1;
	}
	
	public static ItemStack findFirstItem(ItemHandler inv, List<ItemStack> filter, boolean extract)
	{
		boolean emptyFilter = true;
		for (int i=0; i<filter.size(); i++)
		{
			if (!filter.get(i).isEmpty())
			{
				emptyFilter = false;
				break;
			}
		}
		if (emptyFilter) return findFirstItem(inv, extract);
		
		for (int i=0; i<inv.getSlots(); i++)
		{
			ItemStack is = inv.getStackInSlot(i);
			int slot = getFilterSlot(is, filter);
			if (!is.isEmpty() && slot != -1)
			{
				return inv.extractItem(i, filter.get(slot).getCount(), !extract);
			}
		}
		return ItemStack.EMPTY;
	}
	
	public static ItemStack extractFirstItem(ItemHandler inv, List<ItemStack> filter)
	{
		boolean emptyFilter = true;
		for (int i=0; i<filter.size(); i++)
		{
			if (!filter.get(i).isEmpty())
			{
				emptyFilter = false;
				break;
			}
		}
		for (int i=0; i<inv.getSlots(); i++)
		{
			ItemStack is = inv.extractItem(i, inv.getSlotLimit(i), true);
			if (!is.isEmpty())
			{
				if (emptyFilter) return inv.extractItem(i, inv.getSlotLimit(i), false);
				else
				{
					int filterSlot = getFilterSlot(is, filter);
					if (filterSlot != -1) return inv.extractItem(i, filter.get(filterSlot).getCount(), false);
				}
			}
		}
		return ItemStack.EMPTY;
	}
	
	public static ItemStack findFirstItem(ItemHandler inv, boolean extract)
	{
		for (int i=0; i<inv.getSlots(); i++)
		{
			if (!inv.extractItem(i, inv.getSlotLimit(i), true).isEmpty()) return inv.extractItem(i, inv.getSlotLimit(i), !extract);
		}
		return ItemStack.EMPTY;
	}
	
	public static boolean isInventoryEmpty(ItemHandler inv)
	{
		return findFirstItem(inv, false).isEmpty();
	}
	
	public static boolean areItemStacksIdentical(ItemStack a, ItemStack b)
	{
		return (a.isItemEqual(b) && a.getCount() == b.getCount());
	}
	
	public static int getPriority(ItemHandler inv)
	{
		if (inv instanceof BackstuffInventory) return -1;
		return 0;
	}

	public static boolean canStacksCombine(ItemStack a, ItemStack b)
	{
		return (a.isEmpty() || b.isEmpty()) || (a.isItemEqual(b) && a.getCount() < a.getMaxCount() && b.getCount() < b.getMaxCount());
	}
	
	public static boolean isInventorySuitable(ItemHandler inventory, List<ItemStack> filter)
	{
		return !findFirstItem(inventory, filter, false).isEmpty();
	}
}