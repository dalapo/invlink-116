package dalapo.invlink.auxiliary;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;

public interface ILinkable
{
	/**
	 * Links this block to another.
	 * @param other The position to be linked to
	 * @param ep The entity doing the linking
	 * @return <code>true</code> if the link was successful; <code>false</code> otherwise.
	 */
	public boolean link(BlockPos other, PlayerEntity ep);
}