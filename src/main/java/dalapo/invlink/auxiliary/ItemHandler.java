package dalapo.invlink.auxiliary;

import net.minecraft.item.ItemStack;

/**
 * Re-implementation of Forge's IItemHandler interface. This use of the code is covered under Forge's LGPL license.
 */
public interface ItemHandler
{
	public int getSlots();
	public ItemStack getStackInSlot(int slot);
	public void setStackInSlot(int slot, ItemStack is);
	public ItemStack extractItem(int slot, int amount, boolean simulate);
	public ItemStack extractItem(int slot, boolean simulate);
	public ItemStack insertItem(int slot, ItemStack is, boolean simulate);
	public boolean isItemValidForSlot(ItemStack is, int slot);
	public int getSlotLimit(int slot);
}