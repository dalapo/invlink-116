package dalapo.invlink.auxiliary;

import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.screen.ScreenHandler;

public interface IGuiTile
{
	public ScreenHandler getContainer(PlayerEntity ep);
	public HandledScreen<? extends ScreenHandler> getGui(PlayerEntity ep);
}