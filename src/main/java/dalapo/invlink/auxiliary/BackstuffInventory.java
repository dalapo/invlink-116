package dalapo.invlink.auxiliary;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.tile.TileEntityNetworkInserter;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;

public class BackstuffInventory implements ItemHandler
{
	LinkedList<ItemStack> items = new LinkedList<>();
	TileEntityNetworkInserter parent;
	
	public BackstuffInventory(TileEntityNetworkInserter parent)
	{
		this.parent = parent;
	}

	// If signal is true, immediately alert the parent tile to push
	public void addItem(ItemStack stack, boolean signal)
	{
		Logger.info("Adding " + stack + " to backstuff inventory");
		items.add(stack);
		if (signal) parent.forcePushItem();
	}
	
	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		if (!simulate) items.add(slot, stack.copy());
		return ItemStack.EMPTY;
	}

	public ItemStack removeStack(int slot, int amount, boolean simulate)
	{
		ItemStack is = items.get(slot).copy();
		is.setCount(Math.min(amount, is.getCount()));
		if (!simulate) items.get(slot).decrement(amount);
		if (items.get(slot).isEmpty()) items.remove(slot);
		return is;
	}
	
	public ItemStack grabFirstItem(boolean simulate)
	{
		if (simulate) return items.peek();
		else return items.pop();
	}

	@Override
	public int getSlots() {
		// TODO Auto-generated method stub
		return items.size();
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		if (InvLinkMathHelper.isInRange(slot, 0, getSlots())) return items.get(slot);
		return ItemStack.EMPTY;
	}

	@Override
	public void setStackInSlot(int slot, ItemStack is)
	{
		if (InvLinkMathHelper.isInRange(slot, 0, getSlots())) items.set(slot, is.copy());
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		if (InvLinkMathHelper.isInRange(slot, 0, getSlots()))
		{
			ItemStack is = items.get(slot).copy();
			is.setCount(amount);
			if (!simulate) items.get(slot).decrement(amount);
			return is;
		}
		return ItemStack.EMPTY;
	}

	@Override
	public ItemStack extractItem(int slot, boolean simulate)
	{
		if (InvLinkMathHelper.isInRange(slot, 0, getSlots()))
		{
			ItemStack is = items.get(slot).copy();
			if (!simulate) items.set(slot, ItemStack.EMPTY);
			return is;
		}
		return ItemStack.EMPTY;
	}

	@Override
	public boolean isItemValidForSlot(ItemStack is, int slot)
	{
		return true;
	}

	@Override
	public int getSlotLimit(int slot)
	{
		return 64;
	}
}