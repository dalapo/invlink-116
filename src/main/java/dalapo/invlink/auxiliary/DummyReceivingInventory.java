package dalapo.invlink.auxiliary;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;

/**
 * Dummy inventory for the front of network inserters. Accepts items and sends them directly to the backstuff,
 * but only if the backstuff is empty.
 * @author davidpowell
 *
 */
public class DummyReceivingInventory implements Inventory
{
	private BackstuffInventory backstuff;

	public DummyReceivingInventory(BackstuffInventory back)
	{
		backstuff = back;
	}
	
	@Override
	public int size()
	{
		return 1;
	}

	@Override
	public ItemStack getStack(int slot)
	{
		return ItemStack.EMPTY;
	}

	public ItemStack insertItem(int slot, ItemStack stack, boolean simulate)
	{
		if (backstuff.getSlots() == 0)
		{
			if (!simulate) backstuff.addItem(stack, true);
			return ItemStack.EMPTY;
		}
		return stack;
	}

	@Override
	public ItemStack removeStack(int slot)
	{
		return ItemStack.EMPTY;
	}
	
	@Override
	public ItemStack removeStack(int slot, int amount)
	{
		return ItemStack.EMPTY;
	}

	public int getSlotLimit(int slot)
	{
		return 64;
	}

	@Override
	public void clear()
	{
		
	}

	@Override
	public boolean isEmpty()
	{
		return false;
	}

	@Override
	public void setStack(int slot, ItemStack stack)
	{
		
	}

	@Override
	public void markDirty() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean canPlayerUse(PlayerEntity player)
	{
		return false;
	}
}