package dalapo.invlink.auxiliary;

public class Pair<A, B>
{
	public A a;
	public B b;
	
	public Pair()
	{
		
	}
	
	public Pair(A a, B b)
	{
		set(a, b);
	}
	
	public void set(A a, B b)
	{
		this.a = a;
		this.b = b;
	}
	
	@Override
	public boolean equals(Object o)
	{
		if (o instanceof Pair)
		{
			Pair<?, ?> other = (Pair<?, ?>)o;
			return (a.equals(other.a) && b.equals(other.b));
		}
		return false;
	}
	
	public boolean isEquivalent(Pair<?, ?> other)
	{
		return other != null && isEquivalent(other.a, other.b);
	}
	
	public boolean isEquivalent(Object x, Object y)
	{
		return x != null && y != null && (a.equals(x) && b.equals(y)) || (a.equals(y) && b.equals(x));
	}
	
	@Override
	public String toString()
	{
		return String.format("PAIR: {%s ; %s}", a == null ? "null" : a.toString(), b == null ? "null" : b.toString());
	}
}