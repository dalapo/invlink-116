package dalapo.invlink.auxiliary;

import net.minecraft.util.math.Direction;

public interface IInventoriedTile
{
	public ItemHandler getInventory(Direction side);
}