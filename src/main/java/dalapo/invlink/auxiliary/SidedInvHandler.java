package dalapo.invlink.auxiliary;

import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.InventoryHelper;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.Direction;

public class SidedInvHandler implements ItemHandler
{
	SidedInventory inv;
	Direction side;
	
	private int[] slotMap;
	public SidedInvHandler(SidedInventory inv, Direction side)
	{
		this.inv = inv;
		this.side = side;
		slotMap = inv.getAvailableSlots(side);
//		slotMap = new int[slots.length];
//		for (int i=0; i<slots.length; i++)
//		{
//			slotMap[i] = slots[i];
//		}
	}
	
	@Override
	public ItemStack getStackInSlot(int slot)
	{
		if (InvLinkMathHelper.isInRange(slot, 0, slotMap.length)) return inv.getStack(slotMap[slot]);
		return ItemStack.EMPTY;
	}

	@Override
	public int getSlots()
	{
		return slotMap.length;
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		
		if (!InvLinkMathHelper.isInRange(slot, 0, slotMap.length) || amount == 0 || inv.getStack(slotMap[slot]).isEmpty()) return ItemStack.EMPTY;
		ItemStack stack = inv.getStack(slotMap[slot]);
		if (!inv.canExtract(slotMap[slot], stack, side)) return ItemStack.EMPTY;
		if (simulate)
		{
			ItemStack is = inv.getStack(slotMap[slot]).copy();
			is.setCount(Math.min(amount, is.getCount()));
			return is;
		}
		ItemStack is = inv.removeStack(slotMap[slot], amount);
		inv.markDirty();
		return is;
	}

	@Override
	public ItemStack extractItem(int slot, boolean simulate)
	{
		if (!InvLinkMathHelper.isInRange(slot, 0, slotMap.length)) return ItemStack.EMPTY;
		ItemStack stack = inv.getStack(slotMap[slot]);
		if (stack.isEmpty() || !inv.canExtract(slotMap[slot], stack, side)) return ItemStack.EMPTY;
		if (simulate) return inv.getStack(slotMap[slot]).copy();
		else
		{
			ItemStack is = inv.removeStack(slotMap[slot]);
			inv.markDirty();
			return is;
		}
	}

	@Override
	public ItemStack insertItem(int slot, ItemStack is, boolean simulate)
	{
		if (is.isEmpty()) return ItemStack.EMPTY;
		if (!InvLinkMathHelper.isInRange(slot, 0, getSlots()) || !inv.canInsert(slot, is, side) || !InventoryHelper.canStacksCombine(inv.getStack(slotMap[slot]), is)) return is;
		if (inv.getStack(slot).isEmpty())
		{
			inv.setStack(slot, is.copy());
			return ItemStack.EMPTY;
		}
		else
		{
			ItemStack copy = inv.getStack(slot).copy();
			int toTransfer = Math.min(is.getCount(), copy.getMaxCount() - copy.getCount());
			copy.increment(toTransfer);
			if (!simulate) inv.setStack(slot, copy);
			ItemStack toReturn = is.copy();
			toReturn.decrement(toTransfer);
			return toReturn;
		}
	}

	@Override
	public boolean isItemValidForSlot(ItemStack is, int slot)
	{
		return inv.isValid(slot, is);
	}

	@Override
	public int getSlotLimit(int slot)
	{
		return inv.getMaxCountPerStack();
	}

	@Override
	public void setStackInSlot(int slot, ItemStack is)
	{
		if (InvLinkMathHelper.isInRange(slot, 0, slotMap.length)) inv.setStack(slotMap[slot], is);
	}
	
	public SidedInventory getInventory()
	{
		return inv;
	}
}