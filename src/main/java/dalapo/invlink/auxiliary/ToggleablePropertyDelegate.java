package dalapo.invlink.auxiliary;

import dalapo.invlink.helper.Logger;
import net.minecraft.screen.PropertyDelegate;

public interface ToggleablePropertyDelegate extends PropertyDelegate
{
	public default void toggle(int i)
	{
		if (get(i) == 0) set(i, 1);
		else set(i, 0);
	}
}