package dalapo.invlink.auxiliary;

import dalapo.invlink.helper.Logger;

public class ToggleableArrayPropertyDelegate implements ToggleablePropertyDelegate
{
	private final int[] data;
	
	public ToggleableArrayPropertyDelegate(int size)
	{
		data = new int[size];
	}
	
	@Override
	public int get(int index)
	{
		return data[index];
	}

	@Override
	public void set(int index, int value)
	{
		data[index] = value;
		Logger.info("Called set in TAPD; index = " + index + ", value = " + value);
	}

	@Override
	public int size()
	{
		return data.length;
	}
}