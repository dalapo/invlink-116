package dalapo.invlink.auxiliary;

import net.minecraft.block.entity.BlockEntity;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.util.math.Direction;

public class ItemHandlerFactory
{
	// jury-rigged TileEntity.hasCapability(CapabilityItemHandler.ITEM_HANDLER_CAPABILITY, EnumFacing)
	public static boolean handlesItems(BlockEntity te, Direction side)
	{
		if (te instanceof IInventoriedTile && ((IInventoriedTile)te).getInventory(side) != null) return true;
		
		if (te instanceof SidedInventory) return ((SidedInventory)te).getAvailableSlots(side).length > 0;
		else return (te instanceof Inventory);
	}
	
	public static ItemHandler getItemHandler(Inventory inv, Direction side)
	{
		if (inv instanceof IInventoriedTile)
		{
			return ((IInventoriedTile)inv).getInventory(side);
		}
		if (inv instanceof SidedInventory)
		{
			return new SidedInvHandler((SidedInventory)inv, side);
		}
		return new InvHandler(inv);
	}
	
	public static ItemHandler getItemHandlerTile(BlockEntity te, Direction side)
	{
		if (handlesItems(te, side)) return getItemHandler((Inventory)te, side);
		return null;
	}
}