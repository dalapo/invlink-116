package dalapo.invlink.auxiliary;

import net.minecraft.util.math.Direction;

public interface IDirectionalTile
{
	public Direction getDirection();
	public Direction setDirection();
}