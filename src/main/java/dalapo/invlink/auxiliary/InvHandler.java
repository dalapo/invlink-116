package dalapo.invlink.auxiliary;

import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.Logger;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;

public class InvHandler implements ItemHandler
{
	private Inventory inv;
	
	public InvHandler(Inventory inv)
	{
		this.inv = inv;
	}

	@Override
	public ItemStack getStackInSlot(int slot)
	{
		return inv.getStack(slot);
	}
	
	@Override
	public void setStackInSlot(int slot, ItemStack is)
	{
		inv.setStack(slot, is);
	}

	@Override
	public int getSlots()
	{
		return inv.size();
	}

	@Override
	public ItemStack extractItem(int slot, int amount, boolean simulate)
	{
		if (amount == 0 || inv.getStack(slot).isEmpty()) return ItemStack.EMPTY;
		if (simulate)
		{
			ItemStack is = inv.getStack(slot).copy();
			is.setCount(Math.min(amount, is.getCount()));
			return is;
		}
		ItemStack is = inv.removeStack(slot, amount);
		inv.markDirty();
		return is;
	}

	@Override
	public ItemStack extractItem(int slot, boolean simulate)
	{
		if (inv.getStack(slot).isEmpty()) return ItemStack.EMPTY;
		if (simulate) return inv.getStack(slot).copy();
		else
		{
			ItemStack is = inv.removeStack(slot);
			inv.markDirty();
			return is;
		}
	}
	
	@Override
	public ItemStack insertItem(int slot, ItemStack is, boolean simulate)
	{
		if (is.isEmpty() || !InvLinkMathHelper.isInRange(slot, 0, getSlots())) return is;
		if (inv.getStack(slot).isEmpty())
		{
			if (!simulate) inv.setStack(slot, is.copy());
			return ItemStack.EMPTY;
		}
		else if (is.isItemEqual(inv.getStack(slot)))
		{
			ItemStack copy = inv.getStack(slot).copy();
			int toTransfer = Math.min(is.getCount(), copy.getMaxCount() - copy.getCount());
			copy.increment(toTransfer);
			if (!simulate) inv.setStack(slot, copy);
			ItemStack toReturn = is.copy();
			toReturn.decrement(toTransfer);
			return toReturn;
		}
		return is;
	}

	@Override
	public boolean isItemValidForSlot(ItemStack is, int slot)
	{
		return inv.isValid(slot, is);
	}
	
	@Override
	public int getSlotLimit(int slot)
	{
		return inv.getMaxCountPerStack();
	}
	
	public Inventory getInventory()
	{
		return inv;
	}
}