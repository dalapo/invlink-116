package dalapo.invlink.auxiliary;

import net.minecraft.screen.PropertyDelegate;

public class EmptyPropertyDelegate implements PropertyDelegate
{

	@Override
	public int get(int index)
	{
		return 0;
	}

	@Override
	public void set(int index, int value)
	{
	}

	@Override
	public int size()
	{
		return 0;
	}

}
