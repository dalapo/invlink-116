package dalapo.invlink;

import dalapo.invlink.client.gui.ContainerBase;
import dalapo.invlink.client.gui.GuiBasicContainer;
import dalapo.invlink.lookup.BlockRegistry;
import dalapo.invlink.lookup.GuiRegistry;
import dalapo.invlink.lookup.MaterialRegistry;
import dalapo.invlink.lookup.PacketRegistry;
import dalapo.invlink.lookup.TileRegistry;
import dalapo.invlink.client.render.PipeModelLoader;
import dalapo.invlink.client.render.tesr.TesrItemPipe;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.model.ModelLoadingRegistry;
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.util.Identifier;

public class InvLinkClientInit implements ClientModInitializer
{
	public static final Identifier ROUTED_ITEM_PACKET = new Identifier("invlink", "create_item");
	
	@Override
	public void onInitializeClient()
	{
		BlockRenderLayerMap.INSTANCE.putBlock(BlockRegistry.pipe, RenderLayer.getCutout());
		BlockRenderLayerMap.INSTANCE.putBlock(BlockRegistry.detector, RenderLayer.getCutout());
		BlockEntityRendererRegistry.INSTANCE.register(TileRegistry.pipeTile, TesrItemPipe::new);
		BlockEntityRendererRegistry.INSTANCE.register(TileRegistry.detectorTile, TesrItemPipe::new);
		MaterialRegistry.registerMaterials();
		PacketRegistry.registerPacketsS2C();
		GuiRegistry.init();
	}
}