package dalapo.invlink.block;

import java.util.HashMap;
import java.util.Map;

import net.minecraft.block.BlockState;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.WorldAccess;

public abstract class BakedModelBlock extends BlockBase
{
	public static final Map<Direction, BooleanProperty> neighbours = new HashMap<>();
	static {
		for (Direction d : Direction.values())
		{
			neighbours.put(d, BooleanProperty.of(d.asString()));
		}
	}
	
	public BakedModelBlock(Settings settings, String name) 
	{
		super(settings, name);
	}
	
	@Override
	public BlockState getPlacementState(ItemPlacementContext ctx)
	{
		return withConnectionProperties(ctx.getWorld(), ctx.getBlockPos());
	}
	
	private BlockState withConnectionProperties(BlockView world, BlockPos pos)
	{
		BlockState state = getDefaultState();
		for (Direction d : Direction.values())
		{
			state = state.with(neighbours.get(d), neighbouringBlockMeetsCriteria(world, pos.offset(d), d));
		}
		return state;
	}
	
	@Override
	public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState newState, WorldAccess world, BlockPos pos, BlockPos posFrom)
	{
		return state.with(neighbours.get(direction), neighbouringBlockMeetsCriteria(world, pos, direction));
	}

//	@Override
//    protected StateContainer createBlockState()
//	{
//        IProperty[] listedProperties = new IProperty[0]; // no listed properties
//        IUnlistedProperty[] unlistedProperties = new IUnlistedProperty[] {NORTH, SOUTH, WEST, EAST, UP, DOWN};
//        return new ExtendedBlockState(this, listedProperties, unlistedProperties);
//    }
	
//	@Override
//	public BlockState getExtendedState(BlockState state, World world, BlockPos pos)
//	{
//		IExtendedBlockState extendedstate = (IExtendedBlockState)state;
//		boolean north = neighbouringBlockMeetsCriteria(world, pos.north(), Direction.NORTH);
//        boolean south = neighbouringBlockMeetsCriteria(world, pos.south(), Direction.SOUTH);
//        boolean west = neighbouringBlockMeetsCriteria(world, pos.west(), Direction.WEST);
//        boolean east = neighbouringBlockMeetsCriteria(world, pos.east(), Direction.EAST);
//        boolean up = neighbouringBlockMeetsCriteria(world, pos.up(), Direction.UP);
//        boolean down = neighbouringBlockMeetsCriteria(world, pos.down(), Direction.DOWN);
//		return extendedstate.with(NORTH, north).with(SOUTH, south).with(EAST, east).with(WEST, west).with(UP, up).with(DOWN, down);
//	}
	
	protected abstract boolean neighbouringBlockMeetsCriteria(BlockView world, BlockPos pos, Direction f);
}