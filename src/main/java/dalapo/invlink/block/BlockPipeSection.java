package dalapo.invlink.block;

import java.util.HashMap;
import java.util.Map;

import dalapo.invlink.auxiliary.ISpecialItem;
import dalapo.invlink.auxiliary.ItemHandlerFactory;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.logic.INetworkedObject;
import dalapo.invlink.lookup.BlockRegistry;
import dalapo.invlink.lookup.StateList;
import dalapo.invlink.lookup.TileRegistry;
import dalapo.invlink.tile.TileEntityItemPipe;
import net.minecraft.block.Block;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemStack;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;

import net.fabricmc.fabric.mixin.object.builder.AbstractBlockSettingsAccessor;

public class BlockPipeSection extends BlockBase implements BlockEntityProvider, ISpecialItem
{
	private static final Box PIPE_AABB = new Box(0.25, 0.25, 0.25, 0.75, 0.75, 0.75);
	public static final Map<Direction, BooleanProperty> neighbours = new HashMap<>();
	static {
		for (Direction d : Direction.values())
		{
			neighbours.put(d, BooleanProperty.of(d.asString()));
		}
	}
	
	private final boolean hasPower;
	
	public BlockPipeSection(Settings settings, String name, boolean hasPower)
	{
		super(settings, name);
		this.hasPower = hasPower;
		BlockState state = getStateManager().getDefaultState().with(neighbours.get(Direction.NORTH), false).with(neighbours.get(Direction.SOUTH), false).with(neighbours.get(Direction.EAST), false).with(neighbours.get(Direction.WEST), false).with(neighbours.get(Direction.UP), false).with(neighbours.get(Direction.DOWN), false);
		if (hasPower) state = state.with(StateList.power, false);
		setDefaultState(state);
	}
	
	@Override
	protected void appendProperties(StateManager.Builder<Block, BlockState> builder)
	{
//		System.out.println("Entering appendProperties");
		BooleanProperty[] properties = new BooleanProperty[6];
		builder.add(neighbours.values().toArray(properties));
		if (((AbstractBlockSettingsAccessor)settings).getHardness() == 2.5F) // Terrible horrible no good very bad hack
		{
//			System.out.println("Adding power state to detector pipe");
			builder.add(StateList.power);
		}
	}
	
	@Override
	public BlockState getPlacementState(ItemPlacementContext ctx)
	{
		BlockState state = withConnectionProperties(ctx.getWorld(), ctx.getBlockPos());
		if (hasPower) state = state.with(StateList.power, false);
		return state;
	}
	
	private BlockState withConnectionProperties(BlockView world, BlockPos pos)
	{
		BlockState state = stateManager.getDefaultState();
		for (Direction d : Direction.values())
		{
			state = state.with(neighbours.get(d), canConnect(world, pos.offset(d), d));
		}
		return state;
	}
	
	@Override
	public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState newState, WorldAccess world, BlockPos pos, BlockPos posFrom)
	{
		Boolean value = Boolean.valueOf(canConnect(world, pos.offset(direction), direction));
		return state.with(neighbours.get(direction), value);
	}

	@Override
	public BlockEntity createBlockEntity(BlockView worldIn)
	{
		return TileRegistry.getTile(this);
	}
	
	@Override
	public void neighborUpdate(BlockState state, World world, BlockPos pos, Block cause, BlockPos neighbour, boolean notify)
	{
		super.neighborUpdate(state, world, pos, cause, neighbour, notify);
		if (!world.isClient)
		{
			BlockEntity te = world.getBlockEntity(neighbour);
			Direction f = InvLinkMathHelper.getRelativeDirection(pos, neighbour);
			if (te != null && !(te instanceof TileEntityItemPipe))
			{
				TileEntityItemPipe pipe = (TileEntityItemPipe)world.getBlockEntity(pos);
				if (ItemHandlerFactory.handlesItems(te, f.getOpposite()))
				{
					pipe.nonPipeNeighbourAdd(f, ItemHandlerFactory.getItemHandlerTile(te, f.getOpposite()));
				}
				else if (pipe.getInventories().containsKey(f))
				{
					pipe.nonPipeNeighbourRemove(neighbour);
				}
			}
			else if (te == null || !(te instanceof TileEntityItemPipe || ItemHandlerFactory.handlesItems(te, f.getOpposite()))) // Block was removed
			{
				((TileEntityItemPipe)world.getBlockEntity(pos)).nonPipeNeighbourRemove(neighbour);
			}
		}
	}
	
	@Override
	public BlockRenderType getRenderType(BlockState state)
	{
		return BlockRenderType.MODEL;
	}
	
	private boolean canConnect(BlockView world, BlockPos pos, Direction f)
	{
		BlockEntity te = world.getBlockEntity(pos);
		boolean val = te != null && ((te instanceof INetworkedObject && ((INetworkedObject)te).canConnectToSide(f.getOpposite())) || ItemHandlerFactory.handlesItems(te, f.getOpposite()));
//		System.out.println("canConnect: " + val);
		return val;
	}

	@Override
	public String getName(ItemStack is)
	{
		return this.getNameString();
	}
	
//	@Override
//	public int getWeakRedstonePower(BlockState state, BlockView world, BlockPos pos, Direction side)
//	{
//		return state.getValue(StateList.pipetypes) == StateList.PipeType.DETECTOR_ON ? 15 : 0;
//	}
}