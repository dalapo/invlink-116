package dalapo.invlink.block;

import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.lookup.ItemRegistry;
import dalapo.invlink.lookup.StateList;
import dalapo.invlink.tile.TileEntityBase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.StateManager;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

public class BlockDirectional extends BlockBase
{
	public BlockDirectional(Settings settings, String name)
	{
		super(settings, name);
		setDefaultState(getStateManager().getDefaultState().with(StateList.directions, Direction.DOWN));
	}
	
	@Override
	protected void appendProperties(StateManager.Builder<Block, BlockState> stateManager)
	{
		stateManager.add(StateList.directions);
	}
	
	@Override
	public BlockState getPlacementState(ItemPlacementContext ctx)
	{
		boolean sneaking = ctx.getPlayer().isSneaking();
		return (BlockState)this.getDefaultState().with(StateList.directions, sneaking ? ctx.getSide().getOpposite() : ctx.getSide());
	}
	
	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit)
	{
//		if (!world.isClient) Thread.dumpStack();
		if (player.getMainHandStack().isEmpty() && player.isSneaking())
		{
			BlockHelper.rotateBlock(world, pos);
			if (world.getBlockEntity(pos) instanceof TileEntityBase)
			{
				((TileEntityBase)world.getBlockEntity(pos)).updateState();
			}
			return ActionResult.SUCCESS;
		}
		return super.onUse(state, world, pos, player, hand, hit);
	}
}