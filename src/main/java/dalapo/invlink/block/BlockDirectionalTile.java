package dalapo.invlink.block;

import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.lookup.TileRegistry;
import dalapo.invlink.tile.TileEntityBase;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

public class BlockDirectionalTile extends BlockDirectional implements BlockEntityProvider
{
	public BlockDirectionalTile(Settings settings, String name)
	{
		super(settings, name);
	}
	
	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity ep, Hand hand, BlockHitResult result)
	{
		ActionResult res = super.onUse(state, world, pos, ep, hand, result);
		if (!world.isClient) ((TileEntityBase)world.getBlockEntity(pos)).updateState();
		return res;
	}

	@Override
	public BlockEntity createBlockEntity(BlockView world)
	{
		return TileRegistry.getTile(this);
	}
}