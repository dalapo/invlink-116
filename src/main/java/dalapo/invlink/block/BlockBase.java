package dalapo.invlink.block;

import dalapo.invlink.helper.Logger;
import dalapo.invlink.lookup.BlockRegistry;
import dalapo.invlink.tile.TileEntityBase;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Identifier;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockBase extends Block
{
	private final Identifier id;
	public BlockBase(Settings settings, String name)
	{
		super(settings);
		this.id = new Identifier("invlink", name);
		BlockRegistry.addBlock(this);
	}

	public Identifier getID()
	{
		return id;
	}
	
	public String getNameString()
	{
		return getID().getPath();
	}
	
	public NamedScreenHandlerFactory createScreenHandlerFactory(BlockState state, World world, BlockPos pos)
	{
	    BlockEntity blockEntity = world.getBlockEntity(pos);
	    return blockEntity instanceof NamedScreenHandlerFactory ? (NamedScreenHandlerFactory)blockEntity : null;
	}
	
	@Override
	public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit)
	{
		NamedScreenHandlerFactory factory = state.createScreenHandlerFactory(world, pos);
		if (factory != null)
		{
			if (!world.isClient) player.openHandledScreen(factory);
			return ActionResult.SUCCESS;
		}
		return ActionResult.PASS;
	}
	
	@Override
	public void neighborUpdate(BlockState state, World world, BlockPos pos, Block block, BlockPos fromPos, boolean notify)
	{
		BlockEntity te = world.getBlockEntity(pos);
		if (te instanceof TileEntityBase)
		{
			TileEntityBase tile = (TileEntityBase)te;
			
			if (world.isReceivingRedstonePower(pos) && !tile.isPowered)
			{
				tile.onRedstoneEdge();
				tile.isPowered = true;
			}
			else if (!world.isReceivingRedstonePower(pos) && tile.isPowered)
			{
				tile.isPowered = false;
			}
		}
	}
	
	@Override
	public void onBreak(World world, BlockPos pos, BlockState state, PlayerEntity ep)
	{
		BlockEntity te = world.getBlockEntity(pos);
		if (te instanceof TileEntityBase) ((TileEntityBase)te).remove();
		super.onBreak(world, pos, state, ep);
	}
}
