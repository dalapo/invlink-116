package dalapo.invlink;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dalapo.invlink.lookup.BlockRegistry;
import dalapo.invlink.lookup.ItemRegistry;
import dalapo.invlink.lookup.NameList;
import dalapo.invlink.lookup.PacketRegistry;
import dalapo.invlink.lookup.TileRegistry;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.player.AttackBlockCallback;

public class InventoryLink implements ModInitializer
{
	public static final Random random;
	
	public static final String MOD_ID = "invlink";
	public static final Logger logger = LogManager.getLogger(NameList.MOD_ID);
	static {
		random = new Random();
	}
	
	@Override
	public void onInitialize()
	{
		System.out.println("Initializing Inventory Link!");
		BlockRegistry.registerBlocks();
		ItemRegistry.registerItems();
		TileRegistry.registerTiles();
		PacketRegistry.registerPacketsC2S();
	}
}