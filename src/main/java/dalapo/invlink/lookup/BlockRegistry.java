package dalapo.invlink.lookup;

import java.util.ArrayList;
import java.util.List;

import dalapo.invlink.block.BlockBase;
import dalapo.invlink.block.BlockDirectional;
import dalapo.invlink.block.BlockDirectionalTile;
import dalapo.invlink.block.BlockPipeSection;
import dalapo.invlink.helper.RegistryHelper;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Material;

public class BlockRegistry
{
	private static List<BlockBase> blocks = new ArrayList<>();
	
	public static final BlockBase pipe = new BlockPipeSection(FabricBlockSettings.of(Material.METAL).hardness(2.0F).nonOpaque(), "pipe", false);
	public static final BlockBase detector = new BlockPipeSection(FabricBlockSettings.of(Material.METAL).hardness(2.5F).nonOpaque(), "detector", true);
	public static final BlockBase ejector = new BlockDirectionalTile(FabricBlockSettings.of(Material.METAL).hardness(4.0F), "ejector");
	public static final BlockBase retriever = new BlockDirectionalTile(FabricBlockSettings.of(Material.METAL).hardness(4.0F), "retriever");
	public static final BlockBase router = new BlockDirectionalTile(FabricBlockSettings.of(Material.METAL).hardness(2.0F), "router");
	public static final BlockBase teleporter = new BlockDirectionalTile(FabricBlockSettings.of(Material.METAL).hardness(3.0F), "teleport");
	
	public static void addBlock(BlockBase b)
	{
		blocks.add(b);
	}
	
	public static void registerBlocks()
	{
//		System.out.println("Registering blocks");
		for (BlockBase b : blocks)
		{
//			System.out.println("Registering block " + b.getID());
			RegistryHelper.registerBlock(b);
		}
	}
}