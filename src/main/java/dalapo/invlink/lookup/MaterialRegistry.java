package dalapo.invlink.lookup;

import net.fabricmc.fabric.api.renderer.v1.Renderer;
import net.fabricmc.fabric.api.renderer.v1.RendererAccess;
import net.fabricmc.fabric.api.renderer.v1.material.BlendMode;
import net.fabricmc.fabric.api.renderer.v1.material.MaterialFinder;
import net.minecraft.util.Identifier;

public class MaterialRegistry
{
	public static final Identifier PIPE_RENDER_MATERIAL = new Identifier("invlink", "pipe_mat");
	
	public static void registerMaterials()
	{
		Renderer renderer = RendererAccess.INSTANCE.getRenderer();
		MaterialFinder materialFinder = renderer.materialFinder();
		
		renderer.registerMaterial(PIPE_RENDER_MATERIAL, materialFinder.blendMode(0, BlendMode.CUTOUT).find());
	}
}