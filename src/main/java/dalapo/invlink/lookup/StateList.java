package dalapo.invlink.lookup;

import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.EnumProperty;
import net.minecraft.util.StringIdentifiable;
import net.minecraft.util.math.Direction;

public class StateList
{
	public static final EnumProperty<Direction> directions = EnumProperty.of("facing", Direction.class);
	public static final EnumProperty<PipeType> pipetypes = EnumProperty.of("pipetype", PipeType.class);
	public static final BooleanProperty power = BooleanProperty.of("powered");
	
	public static enum PipeType implements StringIdentifiable
	{
		DETECTOR_OFF("det_off"),
		DETECTOR_ON("det_on");
	
		String name;
		PipeType(String name)
		{
			this.name = name;
		}

		@Override
		public String asString()
		{
			return name;
		}
	}
}