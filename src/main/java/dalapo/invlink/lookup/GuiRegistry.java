package dalapo.invlink.lookup;

import dalapo.invlink.InventoryLink;
import dalapo.invlink.auxiliary.ToggleableArrayPropertyDelegate;
import dalapo.invlink.client.gui.ContainerBase;
import dalapo.invlink.client.gui.ContainerFilter;
import dalapo.invlink.client.gui.ContainerInventory;
import dalapo.invlink.client.gui.GuiBasicContainer;
import dalapo.invlink.client.gui.GuiFilter;
import dalapo.invlink.client.gui.GuiRouter;
import dalapo.invlink.client.gui.widget.WidgetToggleSwitch;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry.Factory;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class GuiRegistry
{
	public static final Identifier BASE = new Identifier(InventoryLink.MOD_ID, "base");
	public static final Identifier FILTER = new Identifier(InventoryLink.MOD_ID, "filter");
	public static final Identifier INVENTORY = new Identifier(InventoryLink.MOD_ID, "inventory");
	public static final Identifier ROUTER = new Identifier(InventoryLink.MOD_ID, "router");
//	public static final ScreenHandlerType<ContainerBase> BASE_CONTAINER;
	public static ScreenHandlerType<ContainerFilter> FILTER_CONTAINER;
	public static ScreenHandlerType<ContainerInventory> INVENTORY_CONTAINER;
	public static ScreenHandlerType<ContainerInventory> ROUTER_CONTAINER;
	
	static
	{
//		BASE_CONTAINER = ScreenHandlerRegistry.registerExtended(BASE, ContainerBase::new);
		FILTER_CONTAINER = ScreenHandlerRegistry.registerExtended(FILTER, (id, player, buf) -> new ContainerFilter(id, player, buf, null));
//		INVENTORY_CONTAINER = ScreenHandlerRegistry.registerSimple(INVENTORY, (id, player) -> new ContainerInventory(INVENTORY_CONTAINER, id, 3, 3, player, new SimpleInventory(9), null));
		ROUTER_CONTAINER = ScreenHandlerRegistry.registerExtended(ROUTER, (id, player, buf) -> new ContainerInventory(ROUTER_CONTAINER, id, 1, 1, player, buf, new ToggleableArrayPropertyDelegate(1)));
	}
	
	public static void init()
	{
		ScreenRegistry.register(FILTER_CONTAINER, new Factory<ContainerFilter, GuiFilter>() {

			@Override
			public GuiFilter create(ContainerFilter handler, PlayerInventory inventory, Text title) {
				// TODO Auto-generated method stub
				return new GuiFilter((ContainerFilter)handler, 176, 166, inventory);
			}
		});
		ScreenRegistry.register(ROUTER_CONTAINER, new Factory<ContainerInventory, GuiRouter>() {

			@Override
			public GuiRouter create(ContainerInventory handler, PlayerInventory inventory, Text title) {
				// TODO Auto-generated method stub
				return new GuiRouter("router", "Router", (ContainerInventory)handler, 176, 166, inventory);
			}
		});
//		ScreenRegistry.register(FILTER_CONTAINER, (container, playerInv, title) -> new GuiFilter((ContainerFilter)container, 176, 166, playerInv));
//		ScreenRegistry.register(ROUTER_CONTAINER, (container, playerInv, title) -> new GuiRouter("router", "Router", (ContainerInventory)container, 176, 166, playerInv));
	}
}