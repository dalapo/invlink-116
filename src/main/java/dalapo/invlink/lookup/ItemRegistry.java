package dalapo.invlink.lookup;

import java.util.ArrayList;
import java.util.List;

import dalapo.invlink.helper.RegistryHelper;
import dalapo.invlink.item.ItemBase;
import dalapo.invlink.item.ItemHorseKiller;
import dalapo.invlink.item.ItemLinker;
import dalapo.invlink.item.ItemNetworkDebugger;
import dalapo.invlink.item.ItemNodeDebugger;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ToolMaterials;

public class ItemRegistry
{
	public static List<ItemBase> items = new ArrayList<>();
	
//	public static final ItemBase wrench = new ItemBase(new Item.Settings().group(ItemGroup.TRANSPORTATION), "wrench");
//	public static final ItemHorseKiller horseKiller = new ItemHorseKiller(new Item.Settings().group(ItemGroup.COMBAT).maxDamage(1), "horsekiller");
	public static final ItemBase networkDebugger = new ItemNetworkDebugger(new Item.Settings().group(ItemGroup.TRANSPORTATION), "debug");
	public static final ItemBase nodeDebugger = new ItemNodeDebugger(new Item.Settings().group(ItemGroup.TRANSPORTATION), "nodedebug");
	public static final ItemBase linker = new ItemLinker(new Item.Settings().group(ItemGroup.TRANSPORTATION).maxCount(1), "linker");
	
	public static void addItem(ItemBase item)
	{
		items.add(item);
	}
	
	public static void registerItems()
	{
		for (ItemBase item : items)
		{
			RegistryHelper.registerItem(item);
		}
	}
}
