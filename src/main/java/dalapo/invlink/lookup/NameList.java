package dalapo.invlink.lookup;

public class NameList
{
	private NameList() {}
	
	public static final String MOD_ID = "invlink";
	public static final String MOD_NAME = "Inventory Link";
	public static final String MOD_VERSION = "0.5";
}