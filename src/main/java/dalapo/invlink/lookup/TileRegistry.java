package dalapo.invlink.lookup;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import dalapo.invlink.block.BlockBase;
import dalapo.invlink.tile.*;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.registry.Registry;

public class TileRegistry
{
	public static BlockEntityType<TileEntityItemPipe> pipeTile;
	public static BlockEntityType<TileEntityNotTransposer> ejectorTile;
	public static BlockEntityType<TileEntityNotRetriever> retrieverTile;
	public static BlockEntityType<TileEntityItemPipe> detectorTile;
	public static BlockEntityType<TileEntityRouter> routerTile;
	public static BlockEntityType<TileEntityTeleportPipe> teleporter;
	
	private static Map<BlockBase, BlockEntityType<? extends TileEntityBase>> tileMap = new HashMap<>();
	
	private static <T extends TileEntityBase> BlockEntityType<T> registerTile(BlockBase block, Supplier<T> supplier)
	{
		BlockEntityType<T> bet = Registry.register(Registry.BLOCK_ENTITY_TYPE, "invlink:" + block.getNameString(), BlockEntityType.Builder.create(supplier, block).build(null));
		tileMap.put(block, bet);
		return bet;
	}
	
	public static TileEntityBase getTile(BlockBase block)
	{
		return tileMap.get(block).instantiate();
	}
	
	public static void registerTiles()
	{
		pipeTile = registerTile(BlockRegistry.pipe, TileEntityItemPipe::new);
		ejectorTile = registerTile(BlockRegistry.ejector, TileEntityNotTransposer::new);
		retrieverTile = registerTile(BlockRegistry.retriever, TileEntityNotRetriever::new);
		routerTile = registerTile(BlockRegistry.router, TileEntityRouter::new);
		detectorTile = registerTile(BlockRegistry.detector, TileEntityDetectorPipe::new);
		teleporter = registerTile(BlockRegistry.teleporter, TileEntityTeleportPipe::new);
	}
}