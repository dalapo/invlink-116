package dalapo.invlink.lookup;

import dalapo.invlink.packet.*;
import net.fabricmc.api.EnvType;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.minecraft.util.Identifier;

public class PacketRegistry
{
	public static final Identifier CREATE_ROUTED_ITEM_PACKET = new Identifier("invlink", "s2c_create_item");
	public static final Identifier SEND_CHAT_INFO_PACKET = new Identifier("invlink", "s2c_send_chat_info");
	public static final Identifier TOGGLE_FIELD_PACKET = new Identifier("invlink", "c2s_toggle_field");
	public static final Identifier UPDATE_ROUTED_ITEM_PACKET = new Identifier("invlink", "s2c_update_item");
	public static final Identifier UPGRADE_TILE_PACKET = new Identifier("invlink", "s2c_upgrade_tile");
	
	private static void registerPacket(Identifier id, Class<? extends PacketBase> packet, EnvType side)
	{
		if (side == EnvType.CLIENT) ClientSidePacketRegistry.INSTANCE.register(id, (ctx, data) -> PacketHandler.schedulePacket(ctx, data, packet));
		else ServerSidePacketRegistry.INSTANCE.register(id,  (ctx, data) -> PacketHandler.schedulePacket(ctx, data, packet));
	}
	
	public static void registerPacketsS2C()
	{
		registerPacket(CREATE_ROUTED_ITEM_PACKET, PacketCreateRoutedItem.class, EnvType.CLIENT);
		registerPacket(SEND_CHAT_INFO_PACKET, PacketSendChatInfo.class, EnvType.CLIENT);
		registerPacket(UPDATE_ROUTED_ITEM_PACKET, PacketUpdateRoutedItem.class, EnvType.CLIENT);
		registerPacket(UPGRADE_TILE_PACKET, PacketUpgradeTile.class, EnvType.CLIENT);
	}
	
	public static void registerPacketsC2S()
	{
		registerPacket(TOGGLE_FIELD_PACKET, PacketToggleField.class, EnvType.SERVER);
	}
}