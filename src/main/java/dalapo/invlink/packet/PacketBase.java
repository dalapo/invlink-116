package dalapo.invlink.packet;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Identifier;
import net.minecraft.world.World;

public abstract class PacketBase
{
	private final Identifier id;
	
	public PacketBase(Identifier id)
	{
		this.id = id;
	}
	
	public Identifier getID()
	{
		return id;
	}
	
	public abstract void fromBytes(ByteBuf bytes);
	public abstract void toBytes(ByteBuf bytes);
	protected abstract void handleClient(PacketBase msg, World world, PlayerEntity ep);
	protected abstract void handleServer(PacketBase msg, World world, PlayerEntity ep);
}