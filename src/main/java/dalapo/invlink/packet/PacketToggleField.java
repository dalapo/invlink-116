package dalapo.invlink.packet;

import dalapo.invlink.auxiliary.ToggleablePropertyDelegate;
import dalapo.invlink.client.gui.ContainerBase;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.lookup.PacketRegistry;
import dalapo.invlink.tile.TileEntityBase;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class PacketToggleField extends PacketBase
{
	BlockPos bp;
	int id;
	
	public PacketToggleField(BlockPos bp, int id)
	{
		super(PacketRegistry.TOGGLE_FIELD_PACKET);
		this.bp = bp;
		this.id = id;
	}
	
	public PacketToggleField()
	{
		super(PacketRegistry.TOGGLE_FIELD_PACKET);
	}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		bp = BlockPos.fromLong(buf.readLong());
		id = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeLong(bp.asLong());
		buf.writeInt(id);
	}

	@Override
	protected void handleClient(PacketBase msg, World world, PlayerEntity ep)
	{
		// no-op
	}

	@Override
	protected void handleServer(PacketBase msg, World world, PlayerEntity ep)
	{
		PacketToggleField packet = (PacketToggleField)msg;
		TileEntityBase te = (TileEntityBase)world.getBlockEntity(((PacketToggleField)msg).bp);
		((ToggleablePropertyDelegate)te.getDelegate()).toggle(id);
		te.markDirty();
		Logger.info("Received packet");
	}
	
}