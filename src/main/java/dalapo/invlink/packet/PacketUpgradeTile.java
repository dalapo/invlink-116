package dalapo.invlink.packet;

import dalapo.invlink.auxiliary.IUpgradeable;
import dalapo.invlink.lookup.PacketRegistry;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class PacketUpgradeTile extends PacketBase
{
	BlockPos tilePos;
	int upgradeID;
	
	public PacketUpgradeTile(BlockPos pos, int id)
	{
		super(PacketRegistry.UPGRADE_TILE_PACKET);
		tilePos = pos;
		upgradeID = id;
	}
	
	public PacketUpgradeTile()
	{
		super(PacketRegistry.UPGRADE_TILE_PACKET);
	}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		tilePos = BlockPos.fromLong(buf.readLong());
		upgradeID = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeLong(tilePos.asLong());
		buf.writeInt(upgradeID);
	}

	@Override
	protected void handleClient(PacketBase msg, World world, PlayerEntity ep)
	{
		PacketUpgradeTile packet = (PacketUpgradeTile)msg;
		IUpgradeable te = (IUpgradeable)world.getBlockEntity(packet.tilePos);
		te.applyUpgrade(packet.upgradeID);
	}

	@Override
	protected void handleServer(PacketBase msg, World world, PlayerEntity ep)
	{
		// TODO Auto-generated method stub
		
	}

}
