package dalapo.invlink.packet;

import java.util.stream.Stream;

import io.netty.buffer.Unpooled;
import net.fabricmc.api.EnvType;
import net.fabricmc.fabric.api.network.ClientSidePacketRegistry;
import net.fabricmc.fabric.api.network.PacketContext;
import net.fabricmc.fabric.api.network.ServerSidePacketRegistry;
import net.fabricmc.fabric.api.server.PlayerStream;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class PacketHandler
{
	public static void schedulePacket(PacketContext ctx, PacketByteBuf data, Class<? extends PacketBase> packet)
	{
		try {
			PacketBase msg = packet.newInstance();
			msg.fromBytes(data);
			if (ctx.getPacketEnvironment() == EnvType.CLIENT)
			{
				ctx.getTaskQueue().execute(() -> msg.handleClient(msg, MinecraftClient.getInstance().world, MinecraftClient.getInstance().player));
			}
			else
			{
				ctx.getTaskQueue().execute(() -> msg.handleServer(msg, ctx.getPlayer().getEntityWorld(), ctx.getPlayer()));
			}
		}
		catch (IllegalAccessException | InstantiationException e)
		{
			throw new RuntimeException(e);
		}
	}
	
	public static void initPackets()
	{
		
	}
	
	private static PacketByteBuf getBytes(PacketBase packet)
	{
		PacketByteBuf bytes = new PacketByteBuf(Unpooled.buffer());
		packet.toBytes(bytes);
		return bytes;
	}
	
	public static void sendToAll(PacketBase packet, World world)
	{
		Stream<ServerPlayerEntity> players = PlayerStream.all(world.getServer());
		players.forEach(player -> {
			ServerSidePacketRegistry.INSTANCE.sendToPlayer(player, packet.getID(), getBytes(packet));
		});
	}
	
	public static void sendToWatching(PacketBase packet, World world, BlockPos pos)
	{
		Stream<PlayerEntity> players = PlayerStream.watching(world, pos);
		players.forEach(player -> ServerSidePacketRegistry.INSTANCE.sendToPlayer(player, packet.getID(),getBytes(packet)));
	}
	
	public static void sendToPlayer(PlayerEntity player, PacketBase packet)
	{
		ServerSidePacketRegistry.INSTANCE.sendToPlayer(player, packet.getID(), getBytes(packet));
	}
	
	public static void sendToServer(PacketBase packet)
	{
		ClientSidePacketRegistry.INSTANCE.sendToServer(packet.getID(), getBytes(packet));
	}
}