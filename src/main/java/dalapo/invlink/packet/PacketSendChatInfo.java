package dalapo.invlink.packet;

import dalapo.invlink.helper.ChatHelper;
import dalapo.invlink.lookup.PacketRegistry;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.world.World;

public class PacketSendChatInfo extends PacketBase
{
	String message;
	
	public PacketSendChatInfo(String msg)
	{
		super(PacketRegistry.SEND_CHAT_INFO_PACKET);
		this.message = msg;
	}
	
	public PacketSendChatInfo()
	{
		super(PacketRegistry.SEND_CHAT_INFO_PACKET);
	}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		PacketByteBuf buffer = new PacketByteBuf(buf);
		int length = buffer.readInt();
		message = buffer.readString(length);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		PacketByteBuf buffer = new PacketByteBuf(buf);
		buffer.writeInt(message.length());
		buffer.writeString(message);
	}

	@Override
	protected void handleClient(PacketBase msg, World world, PlayerEntity ep)
	{
		ChatHelper.sendChatToPlayer(ep, ((PacketSendChatInfo)msg).message);
	}

	@Override
	protected void handleServer(PacketBase msg, World world, PlayerEntity ep)
	{
		// no-op
	}
}