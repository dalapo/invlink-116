package dalapo.invlink.packet;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import dalapo.invlink.logic.RoutedItem;
import dalapo.invlink.logic.RoutedItemClient;
import dalapo.invlink.lookup.PacketRegistry;
import dalapo.invlink.tile.TileEntityItemPipe;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

public class PacketCreateRoutedItem extends PacketBase
{
	private ItemStack is;
	private BlockPos pos;
	private Direction inDir;
	private Direction outDir;
	
	public PacketCreateRoutedItem(RoutedItem item)
	{
		super(PacketRegistry.CREATE_ROUTED_ITEM_PACKET);
		is = item.getItem();
		pos = item.getPos();
		inDir = item.getDir();
		outDir = item.getNext();
	}
	
	public PacketCreateRoutedItem()
	{
		super(PacketRegistry.CREATE_ROUTED_ITEM_PACKET);
	}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		PacketByteBuf buffer = new PacketByteBuf(buf);
		is = buffer.readItemStack();
		pos = buffer.readBlockPos();
		inDir = Direction.byId(buffer.readByte());
		byte b = buffer.readByte();
		outDir = b == 99 ? null : Direction.byId(b);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		PacketByteBuf buffer = new PacketByteBuf(buf);
		buffer.writeItemStack(is);
		buffer.writeBlockPos(pos);
		buffer.writeByte(inDir.getId());
		buffer.writeByte(outDir == null ? 99 : outDir.getId());
	}

	@Override
	public void handleClient(PacketBase msg, World world, PlayerEntity ep)
	{
		PacketCreateRoutedItem packet = (PacketCreateRoutedItem)msg;
		RoutedItemClient item = new RoutedItemClient(packet.is, packet.inDir, packet.outDir);
		TileEntityItemPipe te = (TileEntityItemPipe)world.getBlockEntity(packet.pos);
		te.getClientItems().add(item);
	}

	@Override
	public void handleServer(PacketBase msg, World world, PlayerEntity ep)
	{
		// no-op
	}
}