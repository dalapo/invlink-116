package dalapo.invlink.packet;

import java.util.LinkedList;
import java.util.List;

import dalapo.invlink.logic.RoutedItem;
import dalapo.invlink.lookup.PacketRegistry;
import dalapo.invlink.tile.TileEntityItemPipe;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

/**
 * Packet to update a routed item that <i>already exists.</i>
 * @author davidpowell
 *
 */
public class PacketUpdateRoutedItem extends PacketBase
{
	BlockPos pipe;
	int itemID;
	LinkedList<Direction> newSteps = new LinkedList<>();
	
	public PacketUpdateRoutedItem(RoutedItem item)
	{
		super(PacketRegistry.UPDATE_ROUTED_ITEM_PACKET);
		pipe = item.getPos();
		itemID = item.getID();
		newSteps = item.getSteps();
	}
	
	public PacketUpdateRoutedItem()
	{
		super(PacketRegistry.UPDATE_ROUTED_ITEM_PACKET);
	}
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		pipe = BlockPos.fromLong(buf.readLong());
		itemID = buf.readInt();
		int size = buf.readInt();
		for (int i=0; i<size; i++)
		{
			newSteps.add(Direction.byId(buf.readByte()));
		}
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeLong(pipe.asLong());
		buf.writeInt(itemID);
		buf.writeInt(newSteps.size());
		for (Direction f : newSteps)
		{
			buf.writeByte(f.getId());
		}
	}

	@Override
	protected void handleClient(PacketBase msg, World world, PlayerEntity ep)
	{
		PacketUpdateRoutedItem packet = (PacketUpdateRoutedItem)msg;
		TileEntityItemPipe te = (TileEntityItemPipe)world.getBlockEntity(packet.pipe);
		RoutedItem item = te.getItem(packet.itemID);
		item.setPathDirections(packet.newSteps);
	}

	@Override
	protected void handleServer(PacketBase msg, World world, PlayerEntity ep)
	{
		
	}
}