package dalapo.invlink.tile;

import java.util.LinkedList;

import dalapo.invlink.auxiliary.BackstuffInventory;
import dalapo.invlink.auxiliary.DummyReceivingInventory;
import dalapo.invlink.auxiliary.IInventoriedTile;
import dalapo.invlink.auxiliary.IUpgradeable;
import dalapo.invlink.auxiliary.ItemHandler;
import dalapo.invlink.auxiliary.ItemHandlerFactory;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.client.gui.ContainerFilter;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.logic.INetworkedObject;
import dalapo.invlink.logic.PipeNetwork;
import dalapo.invlink.logic.PipeNode;
import dalapo.invlink.logic.RoutedItem;
import dalapo.invlink.packet.PacketCreateRoutedItem;
import dalapo.invlink.packet.PacketHandler;
import dalapo.invlink.lookup.StateList;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.Direction;
import net.minecraft.util.Tickable;
import net.minecraft.util.collection.DefaultedList;

// Abstract base class of Transposer, Retriever, etc
public abstract class TileEntityNetworkInserter extends TileEntityBase implements Inventory, IInventoriedTile, INetworkedObject, Tickable, ExtendedScreenHandlerFactory, IUpgradeable
{
	public TileEntityNetworkInserter(BlockEntityType<?> type)
	{
		super(type);
	}
	private static final int TICKS_PER_PUSH = 8;
	
	protected boolean isAutomatic = false;
	protected Direction direction; // Points towards the pipes it pushes into
	protected PipeNetwork network;
	protected BackstuffInventory inventory = new BackstuffInventory(this);
	protected DummyReceivingInventory receiver = new DummyReceivingInventory(inventory);
	protected DefaultedList<ItemStack> filter = DefaultedList.ofSize(9, ItemStack.EMPTY);
	private int cooldown = 0;
	
	public abstract void performAction();
	
	@Override
	public void onRedstoneEdge()
	{
		Logger.info("Entered onRedstoneEdge");
		performAction();
	}
	
	@Override
	public void updateState()
	{
		direction = world.getBlockState(pos).get(StateList.directions);
	}
	
	public PipeNetwork getNetwork()
	{
		return network;
	}
	
	public void setNetwork(PipeNetwork net)
	{
		this.network = net;
	}

	@Override
	public boolean canConnectToSide(Direction side)
	{
		return direction != null && direction.getAxis() == side.getAxis();
	}
	
	private void pushNextItem()
	{
		if (world.isAir(pos.offset(direction)))
		{
			world.spawnEntity(new ItemEntity(world, pos.offset(direction).getX()+0.5, pos.offset(direction).getY()+0.5, pos.offset(direction).getZ()+0.5, inventory.grabFirstItem(false)));
		}
		else
		{
			BlockEntity te = world.getBlockEntity(getPos().offset(direction));
			if (te instanceof TileEntityItemPipe)
			{
				TileEntityItemPipe pipe = (TileEntityItemPipe)te;
				ItemStack is = inventory.grabFirstItem(true);
				Pair<PipeNode, Direction> dest = pipe.getNetwork().findSuitableDestination(pipe.getNodes().a, is, false);
				if (dest != null)
				{
					RoutedItem routedItem = new RoutedItem(direction, dest, is);
					LinkedList<PipeNode> path = pipe.getNetwork().pathNodal(pipe.getNodes().a, dest.a);
					routedItem.setPath(path);
					pipe.addItem(routedItem);
					pipe.getNetwork().addItem(routedItem);
					inventory.grabFirstItem(false);
					PacketHandler.sendToAll(new PacketCreateRoutedItem(routedItem), world);
				}
			}
			else if (te != null && ItemHandlerFactory.handlesItems(te, direction.getOpposite()))
			{
				ItemStack is = InventoryHelper.tryInsertItem(ItemHandlerFactory.getItemHandler((Inventory)te, direction.getOpposite()), inventory.grabFirstItem(false), false);
				if (!is.isEmpty()) inventory.addItem(is, false);
			}
		}
	}
	
	public void forcePushItem()
	{
		cooldown = 0;
	}
	
	@Override
	public void tick()
	{
		if (direction == null) direction = world.getBlockState(pos).get(StateList.directions);
		if (cooldown-- <= 0)
		{
			if (inventory.getSlots() > 0)
			{
				pushNextItem();
			}
			if (isAutomatic && !world.isReceivingRedstonePower(pos))
			{
				onRedstoneEdge();
			}
			cooldown = TICKS_PER_PUSH;
		}
	}
	
	public void makeAutomatic()
	{
		isAutomatic = true;
	}
	
	@Override
	public CompoundTag toTag(CompoundTag nbt)
	{
		super.toTag(nbt);
		Inventories.toTag(nbt, filter);
		nbt.putBoolean("auto", isAutomatic);
		return nbt;
	}
	
	@Override
	public void fromTag(BlockState state, CompoundTag nbt)
	{
		super.fromTag(state, nbt);
		direction = state.get(StateList.directions);
		Inventories.fromTag(nbt, filter);
		isAutomatic = nbt.getBoolean("auto");
	}
	
	@Override
	public boolean applyUpgrade(int id)
	{
		if (id == 0 && !isAutomatic)
		{
			isAutomatic = true;
			return true;
		}
		return false;
	}

	@Override
	@Environment(EnvType.CLIENT)
	public Text getDisplayName()
	{
		return Text.of("Filter");
	}
	
	@Override
	public int size()
	{
		return filter.size();
	}

	@Override
	public boolean isEmpty()
	{
		for (ItemStack is : filter)
		{
			if (!is.isEmpty()) return false;
		}
		return true;
	}

	@Override
	public ItemStack getStack(int slot)
	{
		if (InvLinkMathHelper.isInRange(slot, 0, filter.size())) return filter.get(slot);
		return ItemStack.EMPTY;
	}

	@Override
	public ItemStack removeStack(int slot, int amount)
	{
		if (getStack(slot).isEmpty()) return ItemStack.EMPTY;
		return getStack(slot).split(amount);
	}

	@Override
	public ItemStack removeStack(int slot) {
		if (getStack(slot).isEmpty()) return ItemStack.EMPTY;
		return getStack(slot).split(getStack(slot).getCount());
	}

	@Override
	public void setStack(int slot, ItemStack stack)
	{
		if (InvLinkMathHelper.isInRange(slot, 0, filter.size()))
		{
			filter.set(slot, stack);
		}
	}

	@Override
	public boolean canPlayerUse(PlayerEntity player)
	{
		return true;
	}

	@Override
	public void clear()
	{
		filter.replaceAll(is -> is = ItemStack.EMPTY);
	}
	
	@Override
	public ItemHandler getInventory(Direction side)
	{
		return inventory;
	}

	@Override
	public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player)
	{
		return new ContainerFilter(syncId, inv, this, null);
	}
	
	@Override
	public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf)
	{
		buf.writeBlockPos(pos);
	}
}