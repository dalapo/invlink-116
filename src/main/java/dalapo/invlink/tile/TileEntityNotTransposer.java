package dalapo.invlink.tile;

import dalapo.invlink.auxiliary.ItemHandler;
import dalapo.invlink.auxiliary.ItemHandlerFactory;
import dalapo.invlink.client.gui.ContainerFilter;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.lookup.TileRegistry;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;

public class TileEntityNotTransposer extends TileEntityNetworkInserter
{
	public TileEntityNotTransposer()
	{
		super(TileRegistry.ejectorTile);
	}

	@Override
	public void performAction()
	{
		if (!world.isClient && inventory.getSlots() == 0)
		{
			Logger.info(String.format("Attempting to get inventory in direction %s at position %s", direction.getOpposite(), pos.offset(direction.getOpposite())));
			BlockEntity te = world.getBlockEntity(pos.offset(direction.getOpposite()));
			if (te != null && ItemHandlerFactory.handlesItems(te, direction))
			{
				ItemHandler inv = ItemHandlerFactory.getItemHandler((Inventory)te, direction);
				ItemStack is = InventoryHelper.extractFirstItem(inv, filter);
				if (!is.isEmpty()) inventory.addItem(is, true);
			}
		}
	}

}