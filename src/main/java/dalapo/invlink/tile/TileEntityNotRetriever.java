package dalapo.invlink.tile;

import java.util.LinkedList;

import dalapo.invlink.auxiliary.ItemHandler;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.logic.PipeNode;
import dalapo.invlink.logic.RoutedItem;
import dalapo.invlink.lookup.TileRegistry;
import dalapo.invlink.packet.PacketCreateRoutedItem;
import dalapo.invlink.packet.PacketHandler;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.util.math.Direction;

public class TileEntityNotRetriever extends TileEntityNetworkInserter
{
	public TileEntityNotRetriever()
	{
		super(TileRegistry.retrieverTile);
	}

	@Override
	public void performAction()
	{
		BlockEntity te = world.getBlockEntity(pos.offset(direction.getOpposite()));
		if (te instanceof TileEntityItemPipe && !world.isClient)
		{
			TileEntityItemPipe pipe = (TileEntityItemPipe)te;
//			Logger.info(pipe.getNodes());
			Pair<PipeNode, Direction> pair = pipe.getNetwork().findSuitableSource(pipe.getNodes().a, filter);
			if (pair != null)
			{
				ItemHandler inv = pair.a.getInventory(pair.b).inventory;
				ItemStack is = InventoryHelper.extractFirstItem(inv, filter);
				PipeNode node = pipe.getNodes().a;
				RoutedItem item = new RoutedItem(pair.b.getOpposite(), new Pair<>(node, direction), is);
				LinkedList<PipeNode> path = pipe.getNetwork().pathNodal(pair.a, pipe.getNodes().a);
				item.setPath(path);
				pair.a.getTile().addItem(item);
				PacketHandler.sendToAll(new PacketCreateRoutedItem(item), world);
			}
		}
	}
}