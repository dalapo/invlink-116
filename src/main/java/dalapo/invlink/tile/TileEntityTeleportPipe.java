package dalapo.invlink.tile;

import dalapo.invlink.InvLinkConfigManager;
import dalapo.invlink.auxiliary.ILinkable;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.helper.ChatHelper;
import dalapo.invlink.helper.InvLinkMathHelper;
import dalapo.invlink.helper.Logger;
import dalapo.invlink.logic.INetworkedObject;
import dalapo.invlink.logic.PipeNetwork;
import dalapo.invlink.logic.PipeNode;
import dalapo.invlink.packet.PacketHandler;
import dalapo.invlink.lookup.StateList;
import dalapo.invlink.lookup.TileRegistry;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.BlockPos;

// Modify TileEntityItemPipe.getConnectionDirections() as follows:
// If it is connected to a teleport pipe, connect it to the corresponding pipe on the other side.
public class TileEntityTeleportPipe extends TileEntityBase implements INetworkedObject, ILinkable
{
	public TileEntityTeleportPipe()
	{
		super(TileRegistry.teleporter);
	}

	BlockPos link;
	TileEntityItemPipe pipe;
	
	public TileEntityTeleportPipe getLink()
	{
		if (link == null) return null;
		return (TileEntityTeleportPipe) world.getBlockEntity(link);
	}
	
	public TileEntityItemPipe getPipe()
	{
		return pipe;
	}
	
	public boolean checkPipe()
	{
		BlockEntity te = world.getBlockEntity(pos.offset(world.getBlockState(pos).get(StateList.directions)));
		if (te instanceof TileEntityItemPipe)
		{
			pipe = (TileEntityItemPipe)te;
			return true;
		}
		return false;
	}
	
	public void onLoad()
	{
		checkPipe();
	}
	
//	private void linkPipe(TileEntityTeleportPipe other, boolean reciprocate)
//	{
//		unlink();
//		other.unlink();
//		link = other.pos;
//		
//		if (checkPipe())
//		{
//			pipe.recalcConnections();
//			pipe.getNodes().a.calcConnections();
//			if (!reciprocate)
//			{
//				pipe.neighbourAdded(world.getBlockState(pos).getValue(StateList.directions).getOpposite(), other.pipe);
//				other.pipe.neighbourAdded(world.getBlockState(link).getValue(StateList.directions).getOpposite(), pipe);
//			}
//		}
//		if (reciprocate) other.linkPipe(this, false);
//		markDirty();
////		Logger.info(String.format("Linked pipe %s to pipe %s", pos, other.pos));
//	}
	
	private void unlink()
	{
		if (pipe != null) pipe.neighbourRemoved(InvLinkMathHelper.getRelativeDirection(pipe.getPos(), pos));
		link = null;
	}
	
	@Override
	public void remove()
	{
		if (link != null) ((TileEntityTeleportPipe)world.getBlockEntity(link)).unlink();
		unlink();
	}
	
	public void linkPipe(TileEntityTeleportPipe other)
	{
		if (!world.isClient)
		{
			unlink();
			link = other.pos;
			other.unlink();
			other.link = pos;
			
			if (checkPipe() && other.checkPipe())
			{
				pipe.recalcConnections();
				if (pipe.getNodes().a != null) pipe.getNodes().a.calcConnections();
				other.pipe.recalcConnections();
				if (other.pipe.getNodes().a != null) other.pipe.getNodes().a.calcConnections();
				pipe.neighbourAdded(BlockHelper.getBlockDirection(world, pos).getOpposite(), other.pipe);
				other.pipe.neighbourAdded(BlockHelper.getBlockDirection(world, other.pos).getOpposite(), pipe);
			}
		}
	}

	@Override
	public PipeNetwork getNetwork()
	{
		// TODO Auto-generated method stub
		return pipe.getNetwork();
	}

	@Override
	public void setNetwork(PipeNetwork net)
	{
		// no-op
	}

	@Override
	public boolean canConnectToSide(Direction side)
	{
		return (side == world.getBlockState(pos).get(StateList.directions));
	}
	
	@Override
	public boolean link(BlockPos other, PlayerEntity ep)
	{
		Logger.info("Calling link");
		BlockEntity te = world.getBlockEntity(other);
		if (te instanceof TileEntityTeleportPipe && !pos.equals(other) && InvLinkMathHelper.getAbsoluteDistance(pos, other) <= InvLinkConfigManager.maxTeleporterRange)
		{
			if (!world.isClient) linkPipe((TileEntityTeleportPipe)te);
			else ChatHelper.sendChatToPlayer(ep, "Successfully linked teleporters!");
			return true;
		}
		else if (InvLinkMathHelper.getAbsoluteDistance(pos, other) >= InvLinkConfigManager.maxTeleporterRange)
		{
			ChatHelper.sendChatToPlayer(ep, "Out of range!");
		}
		else if (pos.equals(other))
		{
			ChatHelper.sendChatToPlayer(ep, "Can't link a teleporter to itself.");
		}
		return false;
	}
	
	@Override
	public CompoundTag toTag(CompoundTag nbt)
	{
		super.toTag(nbt);
		if (link != null) nbt.putLong("link", link.asLong());
		return nbt;
	}
	
	@Override
	public void fromTag(BlockState state, CompoundTag nbt)
	{
		super.fromTag(state, nbt);
		if (nbt.contains("link")) link = BlockPos.fromLong(nbt.getLong("link"));
	}
}