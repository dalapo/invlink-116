package dalapo.invlink.tile;

import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.logic.RoutedItem;
import dalapo.invlink.lookup.StateList;
import dalapo.invlink.lookup.TileRegistry;
import net.minecraft.block.entity.BlockEntityType;

public class TileEntityDetectorPipe extends TileEntityItemPipe
{
	public TileEntityDetectorPipe()
	{
		super(TileRegistry.detectorTile);
	}
	
	@Override
	public void tick()
	{
		super.tick();
	}
	
	@Override
	public void addItem(RoutedItem item)
	{
		super.addItem(item);
		world.setBlockState(pos, world.getBlockState(pos).with(StateList.pipetypes, StateList.PipeType.DETECTOR_ON));
		BlockHelper.updateBlock(world, pos);
		markDirty();
	}
	
	@Override
	public void onRemoval()
	{
		if (numItems() == 0)
		{
			world.setBlockState(pos, world.getBlockState(pos).with(StateList.pipetypes, StateList.PipeType.DETECTOR_OFF));
			BlockHelper.updateBlock(world, pos);
			markDirty();
		}
	}
}