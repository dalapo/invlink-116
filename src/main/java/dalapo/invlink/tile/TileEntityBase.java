package dalapo.invlink.tile;

import dalapo.invlink.auxiliary.ToggleablePropertyDelegate;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.util.math.Direction;

public abstract class TileEntityBase extends BlockEntity
{
	public boolean isPowered = false;
	
	public TileEntityBase(BlockEntityType<?> type)
	{
		super(type);
	}
	
	public void remove()
	{
		// no-op
	}
	
	public void onRedstoneEdge()
	{
		// no-op
	}
	
	public void onNeighbourUpdate(Direction f)
	{
		// no-op
	}
	
	public void updateState()
	{
		// no-op
	}
	
	public PropertyDelegate getDelegate()
	{
		return null;
	}
}