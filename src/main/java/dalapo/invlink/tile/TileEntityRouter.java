package dalapo.invlink.tile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import dalapo.invlink.InventoryLink;
import dalapo.invlink.auxiliary.IGuiTile;
import dalapo.invlink.auxiliary.IInventoriedTile;
import dalapo.invlink.auxiliary.ItemHandler;
import dalapo.invlink.auxiliary.ItemHandlerFactory;
import dalapo.invlink.auxiliary.Pair;
import dalapo.invlink.auxiliary.SidedInvHandler;
import dalapo.invlink.auxiliary.ToggleablePropertyDelegate;
import dalapo.invlink.client.gui.ContainerInventory;
import dalapo.invlink.client.gui.GuiBasicContainer;
import dalapo.invlink.client.gui.widget.WidgetToggleSwitch;
import dalapo.invlink.lookup.GuiRegistry;
import dalapo.invlink.lookup.StateList;
import dalapo.invlink.lookup.TileRegistry;
import dalapo.invlink.helper.ArrayHelper;
import dalapo.invlink.helper.BlockHelper;
import dalapo.invlink.helper.InventoryHelper;
import dalapo.invlink.helper.Logger;
import net.fabricmc.fabric.api.block.BlockAttackInteractionAware;
import net.fabricmc.fabric.api.event.player.AttackBlockCallback;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.nbt.LongTag;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.Tickable;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

import static dalapo.invlink.helper.InvLinkMathHelper.isInRangeIncl;
import static dalapo.invlink.helper.BlockHelper.getBlockDirection;

// Works like FZ Router for the most part, but has a built-in, forced machine filter.
/**
 * @see dalapo.invlink.auxiliary.RouterTileRegistry
 */
public class TileEntityRouter extends TileEntityBase implements Tickable, IInventoriedTile, SidedInventory, AttackBlockCallback, ExtendedScreenHandlerFactory
{
	private static final int TILES_PER_TICK = 1;
	
	private Class<? extends BlockEntity> tileType;
	private List<BlockPos> connectedTiles = new ArrayList<>();
	private boolean extract = true; // true: insert into inventories / false: extract from inventories
	private boolean needsRecalc = false;
	
	private DefaultedList<ItemStack> stacks;
	
	public int getConnectedSize()
	{
		return connectedTiles.size();
	}
	
	public ToggleablePropertyDelegate delegate = new ToggleablePropertyDelegate() {

		@Override
		public int get(int index)
		{
			if (index == 0) return extract ? 1 : 0;
			return 0;
		}

		@Override
		public void set(int index, int value) {
			if (index == 0) extract = (value != 0);
//			Logger.info("Called set; value is now " + value);
		}

		@Override
		public int size() {
			return 1;
		}
	};
	
	
	public void toggleField(int i)
	{
		delegate.toggle(i);
	}
	
	public TileEntityRouter()
	{
		super(TileRegistry.routerTile);
		stacks = DefaultedList.<ItemStack>ofSize(1, ItemStack.EMPTY);
		AttackBlockCallback.EVENT.register(this);
//		MinecraftForge.EVENT_BUS.register(this);
	}
	
	private void recalcHelper(BlockPos cur)
	{
		BlockEntity te = world.getBlockEntity(cur);
		connectedTiles.add(cur);
		for (Direction f : Direction.values())
		{
			if (!connectedTiles.contains(cur.offset(f)))
			{
				BlockEntity next = world.getBlockEntity(cur.offset(f));
				if (next != null && next.getClass() == tileType)
				{
					recalcHelper(cur.offset(f));
				}
			}
		}
	}
	
	private void clearTiles()
	{
		connectedTiles.clear();
	}
	
	public void recalc()
	{
		clearTiles();
		BlockEntity te = world.getBlockEntity(pos.offset(getBlockDirection(world, pos)));
		if (te != null && ItemHandlerFactory.handlesItems(te, getBlockDirection(world, pos).getOpposite()))
		{
			tileType = te.getClass();
			recalcHelper(pos.offset(getBlockDirection(world, pos)));
		}
	}
	
//	@SubscribeEvent
//	public void handleBreak(BreakEvent evt)
//	{
//		if (connectedTiles.contains(evt.getPos())) needsRecalc = true;
//	}
//	
//	@SubscribeEvent
//	public void handlePlace(EntityPlaceEvent evt)
//	{
//		for (Direction f : Direction.values())
//		{
//			BlockPos bp = evt.getPos().offset(f);
//			if (connectedTiles.contains(bp) || bp.equals(pos)) needsRecalc = true;
//		}
//	}
	
	@Override
	public void tick()
	{
		if (!world.isClient)
		{
			if (needsRecalc)
			{
				recalc();
				needsRecalc = false;
			}
			
			if (!connectedTiles.isEmpty() && !needsRecalc)
			{
				for (int t=0; t<TILES_PER_TICK; t++)
				{
					BlockEntity te = world.getBlockEntity(connectedTiles.get(InventoryLink.random.nextInt(connectedTiles.size())));
					if (te != null)
					{
//						Logger.info("Interacting with tile at " + te.getPos());
						ItemHandler inv = ItemHandlerFactory.getItemHandlerTile(te, getBlockDirection(world, pos).getOpposite());
						if (!extract) // no no, is no bad code here
						{
							ItemStack toInsert = getInventory(getBlockDirection(world, pos).getOpposite()).extractItem(0, 1, true);
//							Logger.info("Inserting: " + toInsert);
							ItemStack afterInsertion = InventoryHelper.tryInsertItem(inv, toInsert, false);
//							Logger.info("After inserting: " + afterInsertion);
							if (afterInsertion.isEmpty()) getInventory(getBlockDirection(world, pos).getOpposite()).extractItem(0, 1, false);
						}
						else
						{
							for (int i=0; i<inv.getSlots(); i++)
							{
								ItemStack toExtract = inv.extractItem(i, 1, true);
//								Logger.info(toExtract);
								if (!toExtract.isEmpty() && InventoryHelper.tryInsertItem(getInventory(getBlockDirection(world, pos).getOpposite()), toExtract, false).isEmpty())
								{
									inv.extractItem(i, 1, false);
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	@Override
	public void updateState()
	{
		recalc();
	}
	
	@Override
	public CompoundTag toTag(CompoundTag nbt)
	{
		super.toTag(nbt);
		nbt.putBoolean("extracting", extract);
		ListTag positions = new ListTag();
		for (BlockPos bp : connectedTiles)
		{
			positions.add(LongTag.of(bp.asLong()));
		}
		nbt.put("positions", positions);
		Inventories.toTag(nbt, stacks);
		return nbt;
	}
	
	@Override
	public void fromTag(BlockState state, CompoundTag nbt)
	{
		super.fromTag(state, nbt);
		if (nbt.contains("extracting")) extract = nbt.getBoolean("inserting");
		ListTag positionsList = nbt.getList("positions", 4);
		positionsList.forEach(nbtTag -> connectedTiles.add(BlockPos.fromLong(((LongTag)nbtTag).getLong())));
		Inventories.fromTag(nbt, stacks);
	}

	@Override
	public ItemHandler getInventory(Direction side)
	{
		return new SidedInvHandler(this, side);
	}

	@Override
	public void clear() {
		stacks.set(0, ItemStack.EMPTY);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return stacks.get(0).isEmpty();
	}

	@Override
	public ItemStack getStack(int slot) {
		if (slot == 0) return stacks.get(0);
		return ItemStack.EMPTY;
	}

	@Override
	public ItemStack removeStack(int slot, int amount) {
		if (slot == 0)
		{
			ItemStack is = stacks.get(0).copy();
			is.setCount(amount);
			stacks.get(0).decrement(amount);
			return is;
		}
		return ItemStack.EMPTY;
	}

	@Override
	public ItemStack removeStack(int slot) {
		if (slot == 0)
		{
			ItemStack is = stacks.get(0).copy();
			stacks.set(0, ItemStack.EMPTY);
			return is;
		}
		return ItemStack.EMPTY;
	}

	@Override
	public void setStack(int slot, ItemStack stack) {
		if (slot == 0) stacks.set(0, stack);
	}

	@Override
	public boolean canPlayerUse(PlayerEntity player) {
		return true;
	}

	@Override
	public ActionResult interact(PlayerEntity player, World world, Hand hand, BlockPos pos, Direction direction)
	{
		if (connectedTiles.contains(pos))
		{
			needsRecalc = true;
		}
		return ActionResult.PASS;
	}

	@Override
	public Text getDisplayName()
	{
		return Text.of("Router");
	}

	@Override
	public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player)
	{
		return new ContainerInventory(GuiRegistry.ROUTER_CONTAINER, syncId, 1, 1, inv, this, delegate);
	}

	private static final int[] SLOTS = new int[] {0};
	
	@Override
	public int[] getAvailableSlots(Direction side)
	{
		if (side == getBlockDirection(world, pos)) return ArrayHelper.EMPTY_ARRAY;
		else return SLOTS;
	}

	@Override
	public boolean canInsert(int slot, ItemStack stack, Direction dir)
	{
		return (slot == 0 && dir != getBlockDirection(world, pos));
	}

	@Override
	public boolean canExtract(int slot, ItemStack stack, Direction dir) {
		return (slot == 0 && dir != getBlockDirection(world, pos));
	}

	@Override
	public void writeScreenOpeningData(ServerPlayerEntity player, PacketByteBuf buf)
	{
		buf.writeBlockPos(pos);
	}
	
	@Override
	public PropertyDelegate getDelegate()
	{
		return delegate;
	}
}